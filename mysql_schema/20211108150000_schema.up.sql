-- MySQL dump 10.13  Distrib 8.0.25, for macos11 (x86_64)
--
-- Host: localhost    Database: empty
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `metrics`
--

DROP TABLE IF EXISTS `metrics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `metrics` (
                           `id` int NOT NULL AUTO_INCREMENT,
                           `metric_name` varchar(255) NOT NULL,
                           `verbose_name` varchar(1024) DEFAULT NULL,
                           `metric_type` varchar(32) DEFAULT NULL,
                           `json` text NOT NULL,
                           `description` text,
                           `changed_by_fk` int DEFAULT NULL,
                           `changed_on` datetime DEFAULT NULL,
                           `created_by_fk` int DEFAULT NULL,
                           `created_on` datetime DEFAULT NULL,
                           `d3format` varchar(128) DEFAULT NULL,
                           `warning_text` text,
                           `datasource_id` int DEFAULT NULL,
                           `uuid` binary(16) DEFAULT NULL,
                           PRIMARY KEY (`id`),
                           UNIQUE KEY `uq_metrics_metric_name` (`metric_name`,`datasource_id`),
                           UNIQUE KEY `uq_metrics_uuid` (`uuid`),
                           KEY `changed_by_fk` (`changed_by_fk`),
                           KEY `created_by_fk` (`created_by_fk`),
                           KEY `fk_metrics_datasource_id_datasources` (`datasource_id`),
                           CONSTRAINT `fk_metrics_datasource_id_datasources` FOREIGN KEY (`datasource_id`) REFERENCES `datasources` (`id`),
                           CONSTRAINT `metrics_ibfk_3` FOREIGN KEY (`changed_by_fk`) REFERENCES `ab_user` (`id`),
                           CONSTRAINT `metrics_ibfk_4` FOREIGN KEY (`created_by_fk`) REFERENCES `ab_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-18 13:24:39
-- MySQL dump 10.13  Distrib 8.0.25, for macos11 (x86_64)
--
-- Host: localhost    Database: empty
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ab_permission`
--

DROP TABLE IF EXISTS `ab_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ab_permission` (
                                 `id` int NOT NULL AUTO_INCREMENT,
                                 `name` varchar(100) NOT NULL,
                                 PRIMARY KEY (`id`),
                                 UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=98 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-18 13:24:28
-- MySQL dump 10.13  Distrib 8.0.25, for macos11 (x86_64)
--
-- Host: localhost    Database: empty
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ab_view_menu`
--

DROP TABLE IF EXISTS `ab_view_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ab_view_menu` (
                                `id` int NOT NULL AUTO_INCREMENT,
                                `name` varchar(255) NOT NULL,
                                PRIMARY KEY (`id`),
                                UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=162 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-18 13:24:36
-- MySQL dump 10.13  Distrib 8.0.25, for macos11 (x86_64)
--
-- Host: localhost    Database: empty
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `dashboard_roles`
--

DROP TABLE IF EXISTS `dashboard_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `dashboard_roles` (
                                   `id` int NOT NULL AUTO_INCREMENT,
                                   `role_id` int NOT NULL,
                                   `dashboard_id` int DEFAULT NULL,
                                   PRIMARY KEY (`id`),
                                   KEY `dashboard_id` (`dashboard_id`),
                                   KEY `role_id` (`role_id`),
                                   CONSTRAINT `dashboard_roles_ibfk_1` FOREIGN KEY (`dashboard_id`) REFERENCES `dashboards` (`id`),
                                   CONSTRAINT `dashboard_roles_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `ab_role` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-18 13:24:32
-- MySQL dump 10.13  Distrib 8.0.25, for macos11 (x86_64)
--
-- Host: localhost    Database: empty
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tag`
--

DROP TABLE IF EXISTS `tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tag` (
                       `created_on` datetime DEFAULT NULL,
                       `changed_on` datetime DEFAULT NULL,
                       `id` int NOT NULL AUTO_INCREMENT,
                       `name` varchar(250) DEFAULT NULL,
                       `type` enum('custom','type','owner','favorited_by') DEFAULT NULL,
                       `created_by_fk` int DEFAULT NULL,
                       `changed_by_fk` int DEFAULT NULL,
                       PRIMARY KEY (`id`),
                       UNIQUE KEY `name` (`name`),
                       KEY `created_by_fk` (`created_by_fk`),
                       KEY `changed_by_fk` (`changed_by_fk`),
                       CONSTRAINT `tag_ibfk_1` FOREIGN KEY (`created_by_fk`) REFERENCES `ab_user` (`id`),
                       CONSTRAINT `tag_ibfk_2` FOREIGN KEY (`changed_by_fk`) REFERENCES `ab_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-18 13:24:41
-- MySQL dump 10.13  Distrib 8.0.25, for macos11 (x86_64)
--
-- Host: localhost    Database: empty
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ab_permission_view`
--

DROP TABLE IF EXISTS `ab_permission_view`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ab_permission_view` (
                                      `id` int NOT NULL AUTO_INCREMENT,
                                      `permission_id` int DEFAULT NULL,
                                      `view_menu_id` int DEFAULT NULL,
                                      PRIMARY KEY (`id`),
                                      UNIQUE KEY `permission_id` (`permission_id`,`view_menu_id`),
                                      KEY `view_menu_id` (`view_menu_id`),
                                      CONSTRAINT `ab_permission_view_ibfk_1` FOREIGN KEY (`permission_id`) REFERENCES `ab_permission` (`id`),
                                      CONSTRAINT `ab_permission_view_ibfk_2` FOREIGN KEY (`view_menu_id`) REFERENCES `ab_view_menu` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=314 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-18 13:24:31
-- MySQL dump 10.13  Distrib 8.0.25, for macos11 (x86_64)
--
-- Host: localhost    Database: empty
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ab_permission_view_role`
--

DROP TABLE IF EXISTS `ab_permission_view_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ab_permission_view_role` (
                                           `id` int NOT NULL AUTO_INCREMENT,
                                           `permission_view_id` int DEFAULT NULL,
                                           `role_id` int DEFAULT NULL,
                                           PRIMARY KEY (`id`),
                                           UNIQUE KEY `permission_view_id` (`permission_view_id`,`role_id`),
                                           KEY `role_id` (`role_id`),
                                           CONSTRAINT `ab_permission_view_role_ibfk_1` FOREIGN KEY (`permission_view_id`) REFERENCES `ab_permission_view` (`id`),
                                           CONSTRAINT `ab_permission_view_role_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `ab_role` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=930 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-18 13:24:30
-- MySQL dump 10.13  Distrib 8.0.25, for macos11 (x86_64)
--
-- Host: localhost    Database: empty
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `logs`
--

DROP TABLE IF EXISTS `logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `logs` (
                        `id` int NOT NULL AUTO_INCREMENT,
                        `action` varchar(512) DEFAULT NULL,
                        `user_id` int DEFAULT NULL,
                        `json` text,
                        `dttm` datetime DEFAULT NULL,
                        `dashboard_id` int DEFAULT NULL,
                        `slice_id` int DEFAULT NULL,
                        `duration_ms` int DEFAULT NULL,
                        `referrer` varchar(1024) DEFAULT NULL,
                        PRIMARY KEY (`id`),
                        KEY `user_id` (`user_id`),
                        CONSTRAINT `logs_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `ab_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5990 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-18 13:24:34
-- MySQL dump 10.13  Distrib 8.0.25, for macos11 (x86_64)
--
-- Host: localhost    Database: empty
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `slices`
--

DROP TABLE IF EXISTS `slices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `slices` (
                          `created_on` datetime DEFAULT NULL,
                          `changed_on` datetime DEFAULT NULL,
                          `id` int NOT NULL AUTO_INCREMENT,
                          `slice_name` varchar(250) DEFAULT NULL,
                          `druid_datasource_id` int DEFAULT NULL,
                          `table_id` int DEFAULT NULL,
                          `datasource_type` varchar(200) DEFAULT NULL,
                          `datasource_name` varchar(2000) DEFAULT NULL,
                          `viz_type` varchar(250) DEFAULT NULL,
                          `params` text,
                          `created_by_fk` int DEFAULT NULL,
                          `changed_by_fk` int DEFAULT NULL,
                          `description` text,
                          `cache_timeout` int DEFAULT NULL,
                          `perm` varchar(2000) DEFAULT NULL,
                          `datasource_id` int DEFAULT NULL,
                          `schema_perm` varchar(1000) DEFAULT NULL,
                          `uuid` binary(16) DEFAULT NULL,
                          `query_context` text,
                          `last_saved_at` datetime DEFAULT NULL,
                          `last_saved_by_fk` int DEFAULT NULL,
                          PRIMARY KEY (`id`),
                          UNIQUE KEY `uq_slices_uuid` (`uuid`),
                          KEY `druid_datasource_id` (`druid_datasource_id`),
                          KEY `table_id` (`table_id`),
                          KEY `created_by_fk` (`created_by_fk`),
                          KEY `changed_by_fk` (`changed_by_fk`),
                          KEY `slices_last_saved_by_fk` (`last_saved_by_fk`),
                          CONSTRAINT `slices_ibfk_1` FOREIGN KEY (`druid_datasource_id`) REFERENCES `datasources` (`id`),
                          CONSTRAINT `slices_ibfk_2` FOREIGN KEY (`table_id`) REFERENCES `tables` (`id`),
                          CONSTRAINT `slices_ibfk_3` FOREIGN KEY (`created_by_fk`) REFERENCES `ab_user` (`id`),
                          CONSTRAINT `slices_ibfk_4` FOREIGN KEY (`changed_by_fk`) REFERENCES `ab_user` (`id`),
                          CONSTRAINT `slices_last_saved_by_fk` FOREIGN KEY (`last_saved_by_fk`) REFERENCES `ab_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=412 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-18 13:24:34
-- MySQL dump 10.13  Distrib 8.0.25, for macos11 (x86_64)
--
-- Host: localhost    Database: empty
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `alerts`
--

DROP TABLE IF EXISTS `alerts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `alerts` (
                          `id` int NOT NULL AUTO_INCREMENT,
                          `label` varchar(150) NOT NULL,
                          `active` tinyint(1) DEFAULT NULL,
                          `crontab` varchar(50) NOT NULL,
                          `alert_type` varchar(50) DEFAULT NULL,
                          `log_retention` int NOT NULL,
                          `grace_period` int NOT NULL,
                          `recipients` text,
                          `slice_id` int DEFAULT NULL,
                          `dashboard_id` int DEFAULT NULL,
                          `last_eval_dttm` datetime DEFAULT NULL,
                          `last_state` varchar(10) DEFAULT NULL,
                          `slack_channel` text,
                          `changed_by_fk` int DEFAULT NULL,
                          `changed_on` datetime DEFAULT NULL,
                          `created_by_fk` int DEFAULT NULL,
                          `created_on` datetime DEFAULT NULL,
                          `validator_config` text,
                          `database_id` int NOT NULL,
                          `sql` text NOT NULL,
                          `validator_type` varchar(100) NOT NULL,
                          PRIMARY KEY (`id`),
                          KEY `dashboard_id` (`dashboard_id`),
                          KEY `slice_id` (`slice_id`),
                          KEY `ix_alerts_active` (`active`),
                          KEY `alerts_ibfk_3` (`changed_by_fk`),
                          KEY `alerts_ibfk_4` (`created_by_fk`),
                          KEY `database_id` (`database_id`),
                          CONSTRAINT `alerts_ibfk_1` FOREIGN KEY (`dashboard_id`) REFERENCES `dashboards` (`id`),
                          CONSTRAINT `alerts_ibfk_2` FOREIGN KEY (`slice_id`) REFERENCES `slices` (`id`),
                          CONSTRAINT `alerts_ibfk_3` FOREIGN KEY (`changed_by_fk`) REFERENCES `ab_user` (`id`),
                          CONSTRAINT `alerts_ibfk_4` FOREIGN KEY (`created_by_fk`) REFERENCES `ab_user` (`id`),
                          CONSTRAINT `alerts_ibfk_5` FOREIGN KEY (`database_id`) REFERENCES `dbs` (`id`),
                          CONSTRAINT `alerts_chk_1` CHECK ((`active` in (0,1)))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-18 13:24:29
-- MySQL dump 10.13  Distrib 8.0.25, for macos11 (x86_64)
--
-- Host: localhost    Database: empty
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Slack Messages`
--

DROP TABLE IF EXISTS `Slack Messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Slack Messages` (
                                  `blocks` text,
                                  `bot_id` varchar(255) DEFAULT NULL,
                                  `bot_profile__app_id` varchar(255) DEFAULT NULL,
                                  `bot_profile__deleted` tinyint(1) DEFAULT NULL,
                                  `bot_profile__id` varchar(255) DEFAULT NULL,
                                  `bot_profile__name` varchar(255) DEFAULT NULL,
                                  `bot_profile__team_id` varchar(255) DEFAULT NULL,
                                  `bot_profile__updated` datetime DEFAULT NULL,
                                  `channel_id` varchar(255) DEFAULT NULL,
                                  `client_msg_id` varchar(255) DEFAULT NULL,
                                  `display_as_bot` tinyint(1) DEFAULT NULL,
                                  `file_id` varchar(255) DEFAULT NULL,
                                  `file_ids` text,
                                  `icons__emoji` varchar(255) DEFAULT NULL,
                                  `inviter` varchar(255) DEFAULT NULL,
                                  `is_delayed_message` tinyint(1) DEFAULT NULL,
                                  `is_intro` tinyint(1) DEFAULT NULL,
                                  `is_starred` tinyint(1) DEFAULT NULL,
                                  `last_read` datetime DEFAULT NULL,
                                  `latest_reply` varchar(255) DEFAULT NULL,
                                  `name` varchar(255) DEFAULT NULL,
                                  `old_name` varchar(255) DEFAULT NULL,
                                  `parent_user_id` varchar(255) DEFAULT NULL,
                                  `permalink` varchar(255) DEFAULT NULL,
                                  `pinned_to` text,
                                  `purpose` varchar(255) DEFAULT NULL,
                                  `reactions` text,
                                  `reply_count` bigint DEFAULT NULL,
                                  `reply_users` text,
                                  `reply_users_count` bigint DEFAULT NULL,
                                  `source_team` varchar(255) DEFAULT NULL,
                                  `subscribed` tinyint(1) DEFAULT NULL,
                                  `subtype` varchar(255) DEFAULT NULL,
                                  `team` varchar(255) DEFAULT NULL,
                                  `text` text,
                                  `thread_ts` varchar(255) DEFAULT NULL,
                                  `topic` varchar(255) DEFAULT NULL,
                                  `ts` datetime DEFAULT NULL,
                                  `type` varchar(255) DEFAULT NULL,
                                  `unread_count` bigint DEFAULT NULL,
                                  `upload` tinyint(1) DEFAULT NULL,
                                  `user` varchar(255) DEFAULT NULL,
                                  `user_team` varchar(255) DEFAULT NULL,
                                  `username` varchar(255) DEFAULT NULL,
                                  CONSTRAINT `slack messages_chk_1` CHECK ((`bot_profile__deleted` in (0,1))),
                                  CONSTRAINT `slack messages_chk_2` CHECK ((`display_as_bot` in (0,1))),
                                  CONSTRAINT `slack messages_chk_3` CHECK ((`is_delayed_message` in (0,1))),
                                  CONSTRAINT `slack messages_chk_4` CHECK ((`is_intro` in (0,1))),
                                  CONSTRAINT `slack messages_chk_5` CHECK ((`is_starred` in (0,1))),
                                  CONSTRAINT `slack messages_chk_6` CHECK ((`subscribed` in (0,1))),
                                  CONSTRAINT `slack messages_chk_7` CHECK ((`upload` in (0,1)))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-18 13:24:38
-- MySQL dump 10.13  Distrib 8.0.25, for macos11 (x86_64)
--
-- Host: localhost    Database: empty
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `row_level_security_filters`
--

DROP TABLE IF EXISTS `row_level_security_filters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `row_level_security_filters` (
                                              `created_on` datetime DEFAULT NULL,
                                              `changed_on` datetime DEFAULT NULL,
                                              `id` int NOT NULL AUTO_INCREMENT,
                                              `clause` text NOT NULL,
                                              `created_by_fk` int DEFAULT NULL,
                                              `changed_by_fk` int DEFAULT NULL,
                                              `filter_type` varchar(255) DEFAULT NULL,
                                              `group_key` varchar(255) DEFAULT NULL,
                                              PRIMARY KEY (`id`),
                                              KEY `changed_by_fk` (`changed_by_fk`),
                                              KEY `created_by_fk` (`created_by_fk`),
                                              KEY `ix_row_level_security_filters_filter_type` (`filter_type`),
                                              CONSTRAINT `row_level_security_filters_ibfk_1` FOREIGN KEY (`changed_by_fk`) REFERENCES `ab_user` (`id`),
                                              CONSTRAINT `row_level_security_filters_ibfk_2` FOREIGN KEY (`created_by_fk`) REFERENCES `ab_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-18 13:24:38
-- MySQL dump 10.13  Distrib 8.0.25, for macos11 (x86_64)
--
-- Host: localhost    Database: empty
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `sql_observations`
--

DROP TABLE IF EXISTS `sql_observations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sql_observations` (
                                    `id` int NOT NULL AUTO_INCREMENT,
                                    `dttm` datetime DEFAULT NULL,
                                    `alert_id` int DEFAULT NULL,
                                    `value` float DEFAULT NULL,
                                    `error_msg` varchar(500) DEFAULT NULL,
                                    PRIMARY KEY (`id`),
                                    KEY `alert_id` (`alert_id`),
                                    KEY `ix_sql_observations_dttm` (`dttm`),
                                    CONSTRAINT `sql_observations_ibfk_1` FOREIGN KEY (`alert_id`) REFERENCES `alerts` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-18 13:24:33
-- MySQL dump 10.13  Distrib 8.0.25, for macos11 (x86_64)
--
-- Host: localhost    Database: empty
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `dashboards`
--

DROP TABLE IF EXISTS `dashboards`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `dashboards` (
                              `created_on` datetime DEFAULT NULL,
                              `changed_on` datetime DEFAULT NULL,
                              `id` int NOT NULL AUTO_INCREMENT,
                              `dashboard_title` varchar(500) DEFAULT NULL,
                              `position_json` mediumtext,
                              `created_by_fk` int DEFAULT NULL,
                              `changed_by_fk` int DEFAULT NULL,
                              `css` text,
                              `description` text,
                              `slug` varchar(255) DEFAULT NULL,
                              `json_metadata` text,
                              `published` tinyint(1) DEFAULT NULL,
                              `uuid` binary(16) DEFAULT NULL,
                              PRIMARY KEY (`id`),
                              UNIQUE KEY `idx_unique_slug` (`slug`),
                              UNIQUE KEY `uq_dashboards_uuid` (`uuid`),
                              KEY `created_by_fk` (`created_by_fk`),
                              KEY `changed_by_fk` (`changed_by_fk`),
                              CONSTRAINT `dashboards_ibfk_1` FOREIGN KEY (`created_by_fk`) REFERENCES `ab_user` (`id`),
                              CONSTRAINT `dashboards_ibfk_2` FOREIGN KEY (`changed_by_fk`) REFERENCES `ab_user` (`id`),
                              CONSTRAINT `dashboards_chk_1` CHECK ((`published` in (0,1)))
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-18 13:24:42
-- MySQL dump 10.13  Distrib 8.0.25, for macos11 (x86_64)
--
-- Host: localhost    Database: empty
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ab_register_user`
--

DROP TABLE IF EXISTS `ab_register_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ab_register_user` (
                                    `id` int NOT NULL AUTO_INCREMENT,
                                    `first_name` varchar(64) NOT NULL,
                                    `last_name` varchar(64) NOT NULL,
                                    `username` varchar(64) NOT NULL,
                                    `password` varchar(256) DEFAULT NULL,
                                    `email` varchar(64) NOT NULL,
                                    `registration_date` datetime DEFAULT NULL,
                                    `registration_hash` varchar(256) DEFAULT NULL,
                                    PRIMARY KEY (`id`),
                                    UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-18 13:24:29
-- MySQL dump 10.13  Distrib 8.0.25, for macos11 (x86_64)
--
-- Host: localhost    Database: empty
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `sqlatable_user`
--

DROP TABLE IF EXISTS `sqlatable_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sqlatable_user` (
                                  `id` int NOT NULL AUTO_INCREMENT,
                                  `user_id` int DEFAULT NULL,
                                  `table_id` int DEFAULT NULL,
                                  PRIMARY KEY (`id`),
                                  KEY `table_id` (`table_id`),
                                  KEY `user_id` (`user_id`),
                                  CONSTRAINT `sqlatable_user_ibfk_1` FOREIGN KEY (`table_id`) REFERENCES `tables` (`id`),
                                  CONSTRAINT `sqlatable_user_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `ab_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-18 13:24:40
-- MySQL dump 10.13  Distrib 8.0.25, for macos11 (x86_64)
--
-- Host: localhost    Database: empty
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `report_execution_log`
--

DROP TABLE IF EXISTS `report_execution_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `report_execution_log` (
                                        `id` int NOT NULL AUTO_INCREMENT,
                                        `scheduled_dttm` datetime NOT NULL,
                                        `start_dttm` datetime DEFAULT NULL,
                                        `end_dttm` datetime DEFAULT NULL,
                                        `value` float DEFAULT NULL,
                                        `value_row_json` text,
                                        `state` varchar(50) NOT NULL,
                                        `error_message` text,
                                        `report_schedule_id` int NOT NULL,
                                        `uuid` binary(16) DEFAULT NULL,
                                        PRIMARY KEY (`id`),
                                        KEY `report_schedule_id` (`report_schedule_id`),
                                        CONSTRAINT `report_execution_log_ibfk_1` FOREIGN KEY (`report_schedule_id`) REFERENCES `report_schedule` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-18 13:24:42
-- MySQL dump 10.13  Distrib 8.0.25, for macos11 (x86_64)
--
-- Host: localhost    Database: empty
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `annotation`
--

DROP TABLE IF EXISTS `annotation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `annotation` (
                              `created_on` datetime DEFAULT NULL,
                              `changed_on` datetime DEFAULT NULL,
                              `id` int NOT NULL AUTO_INCREMENT,
                              `start_dttm` datetime DEFAULT NULL,
                              `end_dttm` datetime DEFAULT NULL,
                              `layer_id` int DEFAULT NULL,
                              `short_descr` varchar(500) DEFAULT NULL,
                              `long_descr` text,
                              `changed_by_fk` int DEFAULT NULL,
                              `created_by_fk` int DEFAULT NULL,
                              `json_metadata` text,
                              PRIMARY KEY (`id`),
                              KEY `changed_by_fk` (`changed_by_fk`),
                              KEY `created_by_fk` (`created_by_fk`),
                              KEY `ti_dag_state` (`layer_id`,`start_dttm`,`end_dttm`),
                              CONSTRAINT `annotation_ibfk_1` FOREIGN KEY (`changed_by_fk`) REFERENCES `ab_user` (`id`),
                              CONSTRAINT `annotation_ibfk_2` FOREIGN KEY (`created_by_fk`) REFERENCES `ab_user` (`id`),
                              CONSTRAINT `annotation_ibfk_3` FOREIGN KEY (`layer_id`) REFERENCES `annotation_layer` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-18 13:24:33
-- MySQL dump 10.13  Distrib 8.0.25, for macos11 (x86_64)
--
-- Host: localhost    Database: empty
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `alert_logs`
--

DROP TABLE IF EXISTS `alert_logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `alert_logs` (
                              `id` int NOT NULL AUTO_INCREMENT,
                              `scheduled_dttm` datetime DEFAULT NULL,
                              `dttm_start` datetime DEFAULT NULL,
                              `dttm_end` datetime DEFAULT NULL,
                              `alert_id` int DEFAULT NULL,
                              `state` varchar(10) DEFAULT NULL,
                              PRIMARY KEY (`id`),
                              KEY `alert_id` (`alert_id`),
                              CONSTRAINT `alert_logs_ibfk_1` FOREIGN KEY (`alert_id`) REFERENCES `alerts` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-18 13:24:42
-- MySQL dump 10.13  Distrib 8.0.25, for macos11 (x86_64)
--
-- Host: localhost    Database: empty
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `FCC Survey Results`
--

DROP TABLE IF EXISTS `FCC Survey Results`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `FCC Survey Results` (
                                      `ID` text,
                                      `is_software_dev` float DEFAULT NULL,
                                      `is_first_dev_job` float DEFAULT NULL,
                                      `months_job_search` float DEFAULT NULL,
                                      `job_pref` text,
                                      `job_intr_fllstck` float DEFAULT NULL,
                                      `job_intr_backend` float DEFAULT NULL,
                                      `job_intr_frntend` float DEFAULT NULL,
                                      `job_intr_mobile` float DEFAULT NULL,
                                      `job_intr_devops` float DEFAULT NULL,
                                      `job_intr_datasci` float DEFAULT NULL,
                                      `job_intr_teacher` float DEFAULT NULL,
                                      `job_intr_qa_engn` float DEFAULT NULL,
                                      `job_intr_ux_engn` float DEFAULT NULL,
                                      `job_intr_projm` float DEFAULT NULL,
                                      `job_intr_gamedev` float DEFAULT NULL,
                                      `job_intr_infosec` float DEFAULT NULL,
                                      `job_intr_dataengn` float DEFAULT NULL,
                                      `job_intr_other` text,
                                      `when_appl_job` text,
                                      `expected_earn` float DEFAULT NULL,
                                      `job_lctn_pref` text,
                                      `job_relocate` float DEFAULT NULL,
                                      `reasons_to_code` text,
                                      `reasons_to_code_other` text,
                                      `rsrc_fcc` float DEFAULT NULL,
                                      `rsrc_mdn` float DEFAULT NULL,
                                      `rsrc_so` float DEFAULT NULL,
                                      `rsrc_edx` float DEFAULT NULL,
                                      `rsrc_coursera` float DEFAULT NULL,
                                      `rsrc_khan_acdm` float DEFAULT NULL,
                                      `rsrc_pluralsght` float DEFAULT NULL,
                                      `rsrc_codeacdm` float DEFAULT NULL,
                                      `rsrc_udacity` float DEFAULT NULL,
                                      `rsrc_udemy` float DEFAULT NULL,
                                      `rsrc_code_wars` float DEFAULT NULL,
                                      `rsrc_treehouse` float DEFAULT NULL,
                                      `rsrc_hackerrank` float DEFAULT NULL,
                                      `rsrc_frntendmstr` float DEFAULT NULL,
                                      `rsrc_lynda` float DEFAULT NULL,
                                      `rsrc_egghead` float DEFAULT NULL,
                                      `rsrc_css_tricks` float DEFAULT NULL,
                                      `rsrc_other` text,
                                      `codeevnt_fcc` float DEFAULT NULL,
                                      `codeevnt_hackthn` float DEFAULT NULL,
                                      `codeevnt_confs` float DEFAULT NULL,
                                      `codeevnt_workshps` float DEFAULT NULL,
                                      `codeevnt_startupwknd` float DEFAULT NULL,
                                      `codeevnt_nodeschl` float DEFAULT NULL,
                                      `codeevnt_womenwc` float DEFAULT NULL,
                                      `codeevnt_girldevit` float DEFAULT NULL,
                                      `codeevnt_coderdojo` float DEFAULT NULL,
                                      `codeevnt_meetup` float DEFAULT NULL,
                                      `codeevnt_railsbrdg` float DEFAULT NULL,
                                      `codeevnt_gamejam` float DEFAULT NULL,
                                      `codeevnt_railsgrls` float DEFAULT NULL,
                                      `codeevnt_djangogrls` float DEFAULT NULL,
                                      `codeevnt_wkndbtcmp` float DEFAULT NULL,
                                      `codeevnt_other` text,
                                      `podcast_fcc` float DEFAULT NULL,
                                      `podcast_codenewbie` float DEFAULT NULL,
                                      `podcast_changelog` float DEFAULT NULL,
                                      `podcast_sedaily` float DEFAULT NULL,
                                      `podcast_js_jabber` float DEFAULT NULL,
                                      `podcast_syntaxfm` float DEFAULT NULL,
                                      `podcast_ltcwm` float DEFAULT NULL,
                                      `podcast_fullstckrd` float DEFAULT NULL,
                                      `podcast_frnthppyhr` float DEFAULT NULL,
                                      `podcast_codingblcks` float DEFAULT NULL,
                                      `podcast_shoptalk` float DEFAULT NULL,
                                      `podcast_devtea` float DEFAULT NULL,
                                      `podcast_progthrwdwn` float DEFAULT NULL,
                                      `podcast_geekspeak` float DEFAULT NULL,
                                      `podcast_hanselmnts` float DEFAULT NULL,
                                      `podcast_talkpythonme` float DEFAULT NULL,
                                      `podcast_rubyrogues` float DEFAULT NULL,
                                      `podcast_codepenrd` float DEFAULT NULL,
                                      `podcast_seradio` float DEFAULT NULL,
                                      `podcast_other` text,
                                      `yt_mit_ocw` float DEFAULT NULL,
                                      `yt_fcc` float DEFAULT NULL,
                                      `yt_computerphile` float DEFAULT NULL,
                                      `yt_devtips` float DEFAULT NULL,
                                      `yt_cs_dojo` float DEFAULT NULL,
                                      `yt_engn_truth` float DEFAULT NULL,
                                      `yt_learncodeacdm` float DEFAULT NULL,
                                      `yt_lvluptuts` float DEFAULT NULL,
                                      `yt_funfunfunct` float DEFAULT NULL,
                                      `yt_codingtuts360` float DEFAULT NULL,
                                      `yt_codingtrain` float DEFAULT NULL,
                                      `yt_derekbanas` float DEFAULT NULL,
                                      `yt_simplilearn` float DEFAULT NULL,
                                      `yt_simpleprog` float DEFAULT NULL,
                                      `yt_mozillahacks` float DEFAULT NULL,
                                      `yt_googledevs` float DEFAULT NULL,
                                      `yt_other` text,
                                      `hours_learning` float DEFAULT NULL,
                                      `months_programming` bigint DEFAULT NULL,
                                      `bootcamp_attend` float DEFAULT NULL,
                                      `bootcamp_name` text,
                                      `bootcamp_finished` float DEFAULT NULL,
                                      `bootcamp_have_loan` float DEFAULT NULL,
                                      `bootcamp_recommend` float DEFAULT NULL,
                                      `money_for_learning` float DEFAULT NULL,
                                      `age` bigint DEFAULT NULL,
                                      `gender` text,
                                      `gender_other` text,
                                      `country_citizen` text,
                                      `country_live` text,
                                      `live_city_population` text,
                                      `is_ethnic_minority` float DEFAULT NULL,
                                      `lang_at_home` text,
                                      `school_degree` text,
                                      `school_major` text,
                                      `marital_status` text,
                                      `has_finance_depends` float DEFAULT NULL,
                                      `has_children` float DEFAULT NULL,
                                      `num_children` float DEFAULT NULL,
                                      `do_finance_support` float DEFAULT NULL,
                                      `debt_amt` float DEFAULT NULL,
                                      `home_mrtg_has` float DEFAULT NULL,
                                      `home_mrtg_owe` float DEFAULT NULL,
                                      `student_debt_has` float DEFAULT NULL,
                                      `student_debt_amt` float DEFAULT NULL,
                                      `curr_emplymnt` text,
                                      `curr_emplymnt_other` text,
                                      `curr_field` text,
                                      `last_yr_income` float DEFAULT NULL,
                                      `communite_time` text,
                                      `is_self_employed` float DEFAULT NULL,
                                      `has_served_military` float DEFAULT NULL,
                                      `is_recv_disab_bnft` float DEFAULT NULL,
                                      `has_high_spd_ntnet` float DEFAULT NULL,
                                      `time_start` datetime DEFAULT NULL,
                                      `time_end` text,
                                      `network_id` text,
                                      `time_total_sec` bigint DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-18 13:24:31
-- MySQL dump 10.13  Distrib 8.0.25, for macos11 (x86_64)
--
-- Host: localhost    Database: empty
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `user_attribute`
--

DROP TABLE IF EXISTS `user_attribute`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_attribute` (
                                  `created_on` datetime DEFAULT NULL,
                                  `changed_on` datetime DEFAULT NULL,
                                  `id` int NOT NULL AUTO_INCREMENT,
                                  `user_id` int DEFAULT NULL,
                                  `welcome_dashboard_id` int DEFAULT NULL,
                                  `created_by_fk` int DEFAULT NULL,
                                  `changed_by_fk` int DEFAULT NULL,
                                  PRIMARY KEY (`id`),
                                  KEY `changed_by_fk` (`changed_by_fk`),
                                  KEY `created_by_fk` (`created_by_fk`),
                                  KEY `user_id` (`user_id`),
                                  KEY `welcome_dashboard_id` (`welcome_dashboard_id`),
                                  CONSTRAINT `user_attribute_ibfk_1` FOREIGN KEY (`changed_by_fk`) REFERENCES `ab_user` (`id`),
                                  CONSTRAINT `user_attribute_ibfk_2` FOREIGN KEY (`created_by_fk`) REFERENCES `ab_user` (`id`),
                                  CONSTRAINT `user_attribute_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `ab_user` (`id`),
                                  CONSTRAINT `user_attribute_ibfk_4` FOREIGN KEY (`welcome_dashboard_id`) REFERENCES `dashboards` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-18 13:24:32
-- MySQL dump 10.13  Distrib 8.0.25, for macos11 (x86_64)
--
-- Host: localhost    Database: empty
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `table_schema`
--

DROP TABLE IF EXISTS `table_schema`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `table_schema` (
                                `created_on` datetime DEFAULT NULL,
                                `changed_on` datetime DEFAULT NULL,
                                `extra_json` text,
                                `id` int NOT NULL AUTO_INCREMENT,
                                `tab_state_id` int DEFAULT NULL,
                                `database_id` int NOT NULL,
                                `schema` varchar(256) DEFAULT NULL,
                                `table` varchar(256) DEFAULT NULL,
                                `description` longtext,
                                `expanded` tinyint(1) DEFAULT NULL,
                                `created_by_fk` int DEFAULT NULL,
                                `changed_by_fk` int DEFAULT NULL,
                                PRIMARY KEY (`id`),
                                UNIQUE KEY `ix_table_schema_id` (`id`),
                                KEY `changed_by_fk` (`changed_by_fk`),
                                KEY `created_by_fk` (`created_by_fk`),
                                KEY `database_id` (`database_id`),
                                KEY `tab_state_id` (`tab_state_id`),
                                CONSTRAINT `table_schema_ibfk_1` FOREIGN KEY (`changed_by_fk`) REFERENCES `ab_user` (`id`),
                                CONSTRAINT `table_schema_ibfk_2` FOREIGN KEY (`created_by_fk`) REFERENCES `ab_user` (`id`),
                                CONSTRAINT `table_schema_ibfk_3` FOREIGN KEY (`database_id`) REFERENCES `dbs` (`id`),
                                CONSTRAINT `table_schema_ibfk_4` FOREIGN KEY (`tab_state_id`) REFERENCES `tab_state` (`id`) ON DELETE CASCADE,
                                CONSTRAINT `table_schema_chk_1` CHECK ((`expanded` in (0,1)))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-18 13:24:33
-- MySQL dump 10.13  Distrib 8.0.25, for macos11 (x86_64)
--
-- Host: localhost    Database: empty
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `saved_query`
--

DROP TABLE IF EXISTS `saved_query`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `saved_query` (
                               `created_on` datetime DEFAULT NULL,
                               `changed_on` datetime DEFAULT NULL,
                               `id` int NOT NULL AUTO_INCREMENT,
                               `user_id` int DEFAULT NULL,
                               `db_id` int DEFAULT NULL,
                               `label` varchar(256) DEFAULT NULL,
                               `schema` varchar(128) DEFAULT NULL,
                               `sql` text,
                               `description` text,
                               `changed_by_fk` int DEFAULT NULL,
                               `created_by_fk` int DEFAULT NULL,
                               `extra_json` text,
                               `last_run` datetime DEFAULT NULL,
                               `rows` int DEFAULT NULL,
                               `uuid` binary(16) DEFAULT NULL,
                               PRIMARY KEY (`id`),
                               UNIQUE KEY `uq_saved_query_uuid` (`uuid`),
                               KEY `changed_by_fk` (`changed_by_fk`),
                               KEY `created_by_fk` (`created_by_fk`),
                               KEY `user_id` (`user_id`),
                               KEY `db_id` (`db_id`),
                               CONSTRAINT `saved_query_ibfk_1` FOREIGN KEY (`changed_by_fk`) REFERENCES `ab_user` (`id`),
                               CONSTRAINT `saved_query_ibfk_2` FOREIGN KEY (`created_by_fk`) REFERENCES `ab_user` (`id`),
                               CONSTRAINT `saved_query_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `ab_user` (`id`),
                               CONSTRAINT `saved_query_ibfk_4` FOREIGN KEY (`db_id`) REFERENCES `dbs` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-18 13:24:35
-- MySQL dump 10.13  Distrib 8.0.25, for macos11 (x86_64)
--
-- Host: localhost    Database: empty
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Slack Threads`
--

DROP TABLE IF EXISTS `Slack Threads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Slack Threads` (
                                 `blocks` text,
                                 `channel_id` varchar(255) DEFAULT NULL,
                                 `client_msg_id` varchar(255) DEFAULT NULL,
                                 `latest_reply` varchar(255) DEFAULT NULL,
                                 `reply_count` bigint DEFAULT NULL,
                                 `reply_users` text,
                                 `reply_users_count` bigint DEFAULT NULL,
                                 `subscribed` tinyint(1) DEFAULT NULL,
                                 `team` varchar(255) DEFAULT NULL,
                                 `text` text,
                                 `thread_ts` varchar(255) DEFAULT NULL,
                                 `ts` datetime DEFAULT NULL,
                                 `type` varchar(255) DEFAULT NULL,
                                 `user` varchar(255) DEFAULT NULL,
                                 CONSTRAINT `slack threads_chk_1` CHECK ((`subscribed` in (0,1)))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-18 13:24:37
-- MySQL dump 10.13  Distrib 8.0.25, for macos11 (x86_64)
--
-- Host: localhost    Database: empty
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Vehicle Sales`
--

DROP TABLE IF EXISTS `Vehicle Sales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Vehicle Sales` (
                                 `order_number` bigint DEFAULT NULL,
                                 `quantity_ordered` bigint DEFAULT NULL,
                                 `price_each` float DEFAULT NULL,
                                 `order_line_number` bigint DEFAULT NULL,
                                 `sales` float DEFAULT NULL,
                                 `order_date` datetime DEFAULT NULL,
                                 `status` text,
                                 `quarter` bigint DEFAULT NULL,
                                 `month` bigint DEFAULT NULL,
                                 `year` bigint DEFAULT NULL,
                                 `product_line` text,
                                 `msrp` bigint DEFAULT NULL,
                                 `product_code` text,
                                 `customer_name` text,
                                 `phone` text,
                                 `address_line1` text,
                                 `address_line2` text,
                                 `city` text,
                                 `state` text,
                                 `postal_code` text,
                                 `country` text,
                                 `territory` text,
                                 `contact_last_name` text,
                                 `contact_first_name` text,
                                 `deal_size` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-18 13:24:39
-- MySQL dump 10.13  Distrib 8.0.25, for macos11 (x86_64)
--
-- Host: localhost    Database: empty
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `authorized_email`
--

DROP TABLE IF EXISTS `authorized_email`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `authorized_email` (
                                    `ID` int NOT NULL,
                                    `email` varchar(45) NOT NULL,
                                    PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-18 13:24:43
-- MySQL dump 10.13  Distrib 8.0.25, for macos11 (x86_64)
--
-- Host: localhost    Database: empty
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `dashboard_email_schedules`
--

DROP TABLE IF EXISTS `dashboard_email_schedules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `dashboard_email_schedules` (
                                             `created_on` datetime DEFAULT NULL,
                                             `changed_on` datetime DEFAULT NULL,
                                             `id` int NOT NULL AUTO_INCREMENT,
                                             `active` tinyint(1) DEFAULT NULL,
                                             `crontab` varchar(50) DEFAULT NULL,
                                             `recipients` text,
                                             `deliver_as_group` tinyint(1) DEFAULT NULL,
                                             `delivery_type` enum('attachment','inline') DEFAULT NULL,
                                             `dashboard_id` int DEFAULT NULL,
                                             `created_by_fk` int DEFAULT NULL,
                                             `changed_by_fk` int DEFAULT NULL,
                                             `user_id` int DEFAULT NULL,
                                             `slack_channel` text,
                                             `uuid` binary(16) DEFAULT NULL,
                                             PRIMARY KEY (`id`),
                                             UNIQUE KEY `uq_dashboard_email_schedules_uuid` (`uuid`),
                                             KEY `changed_by_fk` (`changed_by_fk`),
                                             KEY `created_by_fk` (`created_by_fk`),
                                             KEY `dashboard_id` (`dashboard_id`),
                                             KEY `user_id` (`user_id`),
                                             KEY `ix_dashboard_email_schedules_active` (`active`),
                                             CONSTRAINT `dashboard_email_schedules_ibfk_1` FOREIGN KEY (`changed_by_fk`) REFERENCES `ab_user` (`id`),
                                             CONSTRAINT `dashboard_email_schedules_ibfk_2` FOREIGN KEY (`created_by_fk`) REFERENCES `ab_user` (`id`),
                                             CONSTRAINT `dashboard_email_schedules_ibfk_3` FOREIGN KEY (`dashboard_id`) REFERENCES `dashboards` (`id`),
                                             CONSTRAINT `dashboard_email_schedules_ibfk_4` FOREIGN KEY (`user_id`) REFERENCES `ab_user` (`id`),
                                             CONSTRAINT `dashboard_email_schedules_chk_1` CHECK ((`active` in (0,1))),
                                             CONSTRAINT `dashboard_email_schedules_chk_2` CHECK ((`deliver_as_group` in (0,1)))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-18 13:24:28
-- MySQL dump 10.13  Distrib 8.0.25, for macos11 (x86_64)
--
-- Host: localhost    Database: empty
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `query`
--

DROP TABLE IF EXISTS `query`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `query` (
                         `id` int NOT NULL AUTO_INCREMENT,
                         `client_id` varchar(11) NOT NULL,
                         `database_id` int NOT NULL,
                         `tmp_table_name` varchar(256) DEFAULT NULL,
                         `tab_name` varchar(256) DEFAULT NULL,
                         `sql_editor_id` varchar(256) DEFAULT NULL,
                         `user_id` int DEFAULT NULL,
                         `status` varchar(16) DEFAULT NULL,
                         `schema` varchar(256) DEFAULT NULL,
                         `sql` longtext,
                         `select_sql` longtext,
                         `executed_sql` longtext,
                         `limit` int DEFAULT NULL,
                         `select_as_cta` tinyint(1) DEFAULT NULL,
                         `select_as_cta_used` tinyint(1) DEFAULT NULL,
                         `progress` int DEFAULT NULL,
                         `rows` int DEFAULT NULL,
                         `error_message` text,
                         `start_time` decimal(20,6) DEFAULT NULL,
                         `changed_on` datetime DEFAULT NULL,
                         `end_time` decimal(20,6) DEFAULT NULL,
                         `results_key` varchar(64) DEFAULT NULL,
                         `start_running_time` decimal(20,6) DEFAULT NULL,
                         `end_result_backend_time` decimal(20,6) DEFAULT NULL,
                         `tracking_url` text,
                         `extra_json` text,
                         `tmp_schema_name` varchar(256) DEFAULT NULL,
                         `ctas_method` varchar(16) DEFAULT NULL,
                         `limiting_factor` varchar(255) DEFAULT 'UNKNOWN',
                         PRIMARY KEY (`id`),
                         UNIQUE KEY `client_id` (`client_id`),
                         KEY `database_id` (`database_id`),
                         KEY `ti_user_id_changed_on` (`user_id`,`changed_on`),
                         KEY `ix_query_results_key` (`results_key`),
                         CONSTRAINT `query_ibfk_1` FOREIGN KEY (`database_id`) REFERENCES `dbs` (`id`),
                         CONSTRAINT `query_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `ab_user` (`id`),
                         CONSTRAINT `query_chk_2` CHECK ((`select_as_cta` in (0,1))),
                         CONSTRAINT `query_chk_3` CHECK ((`select_as_cta_used` in (0,1)))
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-18 13:24:32
-- MySQL dump 10.13  Distrib 8.0.25, for macos11 (x86_64)
--
-- Host: localhost    Database: empty
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ab_user`
--

DROP TABLE IF EXISTS `ab_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ab_user` (
                           `id` int NOT NULL AUTO_INCREMENT,
                           `first_name` varchar(64) NOT NULL,
                           `last_name` varchar(64) NOT NULL,
                           `username` varchar(64) NOT NULL,
                           `password` varchar(256) DEFAULT NULL,
                           `active` tinyint(1) DEFAULT NULL,
                           `email` varchar(64) NOT NULL,
                           `last_login` datetime DEFAULT NULL,
                           `login_count` int DEFAULT NULL,
                           `fail_login_count` int DEFAULT NULL,
                           `created_on` datetime DEFAULT NULL,
                           `changed_on` datetime DEFAULT NULL,
                           `created_by_fk` int DEFAULT NULL,
                           `changed_by_fk` int DEFAULT NULL,
                           PRIMARY KEY (`id`),
                           UNIQUE KEY `username` (`username`),
                           UNIQUE KEY `email` (`email`),
                           KEY `created_by_fk` (`created_by_fk`),
                           KEY `changed_by_fk` (`changed_by_fk`),
                           CONSTRAINT `ab_user_ibfk_1` FOREIGN KEY (`created_by_fk`) REFERENCES `ab_user` (`id`),
                           CONSTRAINT `ab_user_ibfk_2` FOREIGN KEY (`changed_by_fk`) REFERENCES `ab_user` (`id`),
                           CONSTRAINT `ab_user_chk_1` CHECK ((`active` in (0,1)))
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-18 13:24:30
-- MySQL dump 10.13  Distrib 8.0.25, for macos11 (x86_64)
--
-- Host: localhost    Database: empty
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `annotation_layer`
--

DROP TABLE IF EXISTS `annotation_layer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `annotation_layer` (
                                    `created_on` datetime DEFAULT NULL,
                                    `changed_on` datetime DEFAULT NULL,
                                    `id` int NOT NULL AUTO_INCREMENT,
                                    `name` varchar(250) DEFAULT NULL,
                                    `descr` text,
                                    `changed_by_fk` int DEFAULT NULL,
                                    `created_by_fk` int DEFAULT NULL,
                                    PRIMARY KEY (`id`),
                                    KEY `changed_by_fk` (`changed_by_fk`),
                                    KEY `created_by_fk` (`created_by_fk`),
                                    CONSTRAINT `annotation_layer_ibfk_1` FOREIGN KEY (`changed_by_fk`) REFERENCES `ab_user` (`id`),
                                    CONSTRAINT `annotation_layer_ibfk_2` FOREIGN KEY (`created_by_fk`) REFERENCES `ab_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-18 13:24:32
-- MySQL dump 10.13  Distrib 8.0.25, for macos11 (x86_64)
--
-- Host: localhost    Database: empty
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ab_role`
--

DROP TABLE IF EXISTS `ab_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ab_role` (
                           `id` int NOT NULL AUTO_INCREMENT,
                           `name` varchar(64) NOT NULL,
                           PRIMARY KEY (`id`),
                           UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-18 13:24:34
-- MySQL dump 10.13  Distrib 8.0.25, for macos11 (x86_64)
--
-- Host: localhost    Database: empty
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `World Bank Health Data`
--

DROP TABLE IF EXISTS `World Bank Health Data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `World Bank Health Data` (
                                          `country_name` varchar(255) DEFAULT NULL,
                                          `country_code` varchar(3) DEFAULT NULL,
                                          `region` varchar(255) DEFAULT NULL,
                                          `year` datetime DEFAULT NULL,
                                          `NY_GNP_PCAP_CD` double DEFAULT NULL,
                                          `SE_ADT_1524_LT_FM_ZS` double DEFAULT NULL,
                                          `SE_ADT_1524_LT_MA_ZS` double DEFAULT NULL,
                                          `SE_ADT_1524_LT_ZS` double DEFAULT NULL,
                                          `SE_ADT_LITR_FE_ZS` double DEFAULT NULL,
                                          `SE_ADT_LITR_MA_ZS` double DEFAULT NULL,
                                          `SE_ADT_LITR_ZS` double DEFAULT NULL,
                                          `SE_ENR_ORPH` double DEFAULT NULL,
                                          `SE_PRM_CMPT_FE_ZS` double DEFAULT NULL,
                                          `SE_PRM_CMPT_MA_ZS` double DEFAULT NULL,
                                          `SE_PRM_CMPT_ZS` double DEFAULT NULL,
                                          `SE_PRM_ENRR` double DEFAULT NULL,
                                          `SE_PRM_ENRR_FE` double DEFAULT NULL,
                                          `SE_PRM_ENRR_MA` double DEFAULT NULL,
                                          `SE_PRM_NENR` double DEFAULT NULL,
                                          `SE_PRM_NENR_FE` double DEFAULT NULL,
                                          `SE_PRM_NENR_MA` double DEFAULT NULL,
                                          `SE_SEC_ENRR` double DEFAULT NULL,
                                          `SE_SEC_ENRR_FE` double DEFAULT NULL,
                                          `SE_SEC_ENRR_MA` double DEFAULT NULL,
                                          `SE_SEC_NENR` double DEFAULT NULL,
                                          `SE_SEC_NENR_FE` double DEFAULT NULL,
                                          `SE_SEC_NENR_MA` double DEFAULT NULL,
                                          `SE_TER_ENRR` double DEFAULT NULL,
                                          `SE_TER_ENRR_FE` double DEFAULT NULL,
                                          `SE_XPD_TOTL_GD_ZS` double DEFAULT NULL,
                                          `SH_ANM_CHLD_ZS` double DEFAULT NULL,
                                          `SH_ANM_NPRG_ZS` double DEFAULT NULL,
                                          `SH_CON_1524_FE_ZS` double DEFAULT NULL,
                                          `SH_CON_1524_MA_ZS` double DEFAULT NULL,
                                          `SH_CON_AIDS_FE_ZS` double DEFAULT NULL,
                                          `SH_CON_AIDS_MA_ZS` double DEFAULT NULL,
                                          `SH_DTH_COMM_ZS` double DEFAULT NULL,
                                          `SH_DTH_IMRT` double DEFAULT NULL,
                                          `SH_DTH_INJR_ZS` double DEFAULT NULL,
                                          `SH_DTH_MORT` double DEFAULT NULL,
                                          `SH_DTH_NCOM_ZS` double DEFAULT NULL,
                                          `SH_DTH_NMRT` double DEFAULT NULL,
                                          `SH_DYN_AIDS` double DEFAULT NULL,
                                          `SH_DYN_AIDS_DH` double DEFAULT NULL,
                                          `SH_DYN_AIDS_FE_ZS` double DEFAULT NULL,
                                          `SH_DYN_AIDS_ZS` double DEFAULT NULL,
                                          `SH_DYN_MORT` double DEFAULT NULL,
                                          `SH_DYN_MORT_FE` double DEFAULT NULL,
                                          `SH_DYN_MORT_MA` double DEFAULT NULL,
                                          `SH_DYN_NMRT` double DEFAULT NULL,
                                          `SH_FPL_SATI_ZS` double DEFAULT NULL,
                                          `SH_H2O_SAFE_RU_ZS` double DEFAULT NULL,
                                          `SH_H2O_SAFE_UR_ZS` double DEFAULT NULL,
                                          `SH_H2O_SAFE_ZS` double DEFAULT NULL,
                                          `SH_HIV_0014` double DEFAULT NULL,
                                          `SH_HIV_1524_FE_ZS` double DEFAULT NULL,
                                          `SH_HIV_1524_KW_FE_ZS` double DEFAULT NULL,
                                          `SH_HIV_1524_KW_MA_ZS` double DEFAULT NULL,
                                          `SH_HIV_1524_MA_ZS` double DEFAULT NULL,
                                          `SH_HIV_ARTC_ZS` double DEFAULT NULL,
                                          `SH_HIV_KNOW_FE_ZS` double DEFAULT NULL,
                                          `SH_HIV_KNOW_MA_ZS` double DEFAULT NULL,
                                          `SH_HIV_ORPH` double DEFAULT NULL,
                                          `SH_HIV_TOTL` double DEFAULT NULL,
                                          `SH_IMM_HEPB` double DEFAULT NULL,
                                          `SH_IMM_HIB3` double DEFAULT NULL,
                                          `SH_IMM_IBCG` double DEFAULT NULL,
                                          `SH_IMM_IDPT` double DEFAULT NULL,
                                          `SH_IMM_MEAS` double DEFAULT NULL,
                                          `SH_IMM_POL3` double DEFAULT NULL,
                                          `SH_MED_BEDS_ZS` double DEFAULT NULL,
                                          `SH_MED_CMHW_P3` double DEFAULT NULL,
                                          `SH_MED_NUMW_P3` double DEFAULT NULL,
                                          `SH_MED_PHYS_ZS` double DEFAULT NULL,
                                          `SH_MLR_NETS_ZS` double DEFAULT NULL,
                                          `SH_MLR_PREG_ZS` double DEFAULT NULL,
                                          `SH_MLR_SPF2_ZS` double DEFAULT NULL,
                                          `SH_MLR_TRET_ZS` double DEFAULT NULL,
                                          `SH_MMR_DTHS` double DEFAULT NULL,
                                          `SH_MMR_LEVE` double DEFAULT NULL,
                                          `SH_MMR_RISK` double DEFAULT NULL,
                                          `SH_MMR_RISK_ZS` double DEFAULT NULL,
                                          `SH_MMR_WAGE_ZS` double DEFAULT NULL,
                                          `SH_PRG_ANEM` double DEFAULT NULL,
                                          `SH_PRG_ARTC_ZS` double DEFAULT NULL,
                                          `SH_PRG_SYPH_ZS` double DEFAULT NULL,
                                          `SH_PRV_SMOK_FE` double DEFAULT NULL,
                                          `SH_PRV_SMOK_MA` double DEFAULT NULL,
                                          `SH_STA_ACSN` double DEFAULT NULL,
                                          `SH_STA_ACSN_RU` double DEFAULT NULL,
                                          `SH_STA_ACSN_UR` double DEFAULT NULL,
                                          `SH_STA_ANV4_ZS` double DEFAULT NULL,
                                          `SH_STA_ANVC_ZS` double DEFAULT NULL,
                                          `SH_STA_ARIC_ZS` double DEFAULT NULL,
                                          `SH_STA_BFED_ZS` double DEFAULT NULL,
                                          `SH_STA_BRTC_ZS` double DEFAULT NULL,
                                          `SH_STA_BRTW_ZS` double DEFAULT NULL,
                                          `SH_STA_DIAB_ZS` double DEFAULT NULL,
                                          `SH_STA_IYCF_ZS` double DEFAULT NULL,
                                          `SH_STA_MALN_FE_ZS` double DEFAULT NULL,
                                          `SH_STA_MALN_MA_ZS` double DEFAULT NULL,
                                          `SH_STA_MALN_ZS` double DEFAULT NULL,
                                          `SH_STA_MALR` double DEFAULT NULL,
                                          `SH_STA_MMRT` double DEFAULT NULL,
                                          `SH_STA_MMRT_NE` double DEFAULT NULL,
                                          `SH_STA_ORCF_ZS` double DEFAULT NULL,
                                          `SH_STA_ORTH` double DEFAULT NULL,
                                          `SH_STA_OW15_FE_ZS` double DEFAULT NULL,
                                          `SH_STA_OW15_MA_ZS` double DEFAULT NULL,
                                          `SH_STA_OW15_ZS` double DEFAULT NULL,
                                          `SH_STA_OWGH_FE_ZS` double DEFAULT NULL,
                                          `SH_STA_OWGH_MA_ZS` double DEFAULT NULL,
                                          `SH_STA_OWGH_ZS` double DEFAULT NULL,
                                          `SH_STA_PNVC_ZS` double DEFAULT NULL,
                                          `SH_STA_STNT_FE_ZS` double DEFAULT NULL,
                                          `SH_STA_STNT_MA_ZS` double DEFAULT NULL,
                                          `SH_STA_STNT_ZS` double DEFAULT NULL,
                                          `SH_STA_WAST_FE_ZS` double DEFAULT NULL,
                                          `SH_STA_WAST_MA_ZS` double DEFAULT NULL,
                                          `SH_STA_WAST_ZS` double DEFAULT NULL,
                                          `SH_SVR_WAST_FE_ZS` double DEFAULT NULL,
                                          `SH_SVR_WAST_MA_ZS` double DEFAULT NULL,
                                          `SH_SVR_WAST_ZS` double DEFAULT NULL,
                                          `SH_TBS_CURE_ZS` double DEFAULT NULL,
                                          `SH_TBS_DTEC_ZS` double DEFAULT NULL,
                                          `SH_TBS_INCD` double DEFAULT NULL,
                                          `SH_TBS_MORT` double DEFAULT NULL,
                                          `SH_TBS_PREV` double DEFAULT NULL,
                                          `SH_VAC_TTNS_ZS` double DEFAULT NULL,
                                          `SH_XPD_EXTR_ZS` double DEFAULT NULL,
                                          `SH_XPD_OOPC_TO_ZS` double DEFAULT NULL,
                                          `SH_XPD_OOPC_ZS` double DEFAULT NULL,
                                          `SH_XPD_PCAP` double DEFAULT NULL,
                                          `SH_XPD_PCAP_PP_KD` double DEFAULT NULL,
                                          `SH_XPD_PRIV` double DEFAULT NULL,
                                          `SH_XPD_PRIV_ZS` double DEFAULT NULL,
                                          `SH_XPD_PUBL` double DEFAULT NULL,
                                          `SH_XPD_PUBL_GX_ZS` double DEFAULT NULL,
                                          `SH_XPD_PUBL_ZS` double DEFAULT NULL,
                                          `SH_XPD_TOTL_CD` double DEFAULT NULL,
                                          `SH_XPD_TOTL_ZS` double DEFAULT NULL,
                                          `SI_POV_NAHC` double DEFAULT NULL,
                                          `SI_POV_RUHC` double DEFAULT NULL,
                                          `SI_POV_URHC` double DEFAULT NULL,
                                          `SL_EMP_INSV_FE_ZS` double DEFAULT NULL,
                                          `SL_TLF_TOTL_FE_ZS` double DEFAULT NULL,
                                          `SL_TLF_TOTL_IN` double DEFAULT NULL,
                                          `SL_UEM_TOTL_FE_ZS` double DEFAULT NULL,
                                          `SL_UEM_TOTL_MA_ZS` double DEFAULT NULL,
                                          `SL_UEM_TOTL_ZS` double DEFAULT NULL,
                                          `SM_POP_NETM` double DEFAULT NULL,
                                          `SN_ITK_DEFC` double DEFAULT NULL,
                                          `SN_ITK_DEFC_ZS` double DEFAULT NULL,
                                          `SN_ITK_SALT_ZS` double DEFAULT NULL,
                                          `SN_ITK_VITA_ZS` double DEFAULT NULL,
                                          `SP_ADO_TFRT` double DEFAULT NULL,
                                          `SP_DYN_AMRT_FE` double DEFAULT NULL,
                                          `SP_DYN_AMRT_MA` double DEFAULT NULL,
                                          `SP_DYN_CBRT_IN` double DEFAULT NULL,
                                          `SP_DYN_CDRT_IN` double DEFAULT NULL,
                                          `SP_DYN_CONU_ZS` double DEFAULT NULL,
                                          `SP_DYN_IMRT_FE_IN` double DEFAULT NULL,
                                          `SP_DYN_IMRT_IN` double DEFAULT NULL,
                                          `SP_DYN_IMRT_MA_IN` double DEFAULT NULL,
                                          `SP_DYN_LE00_FE_IN` double DEFAULT NULL,
                                          `SP_DYN_LE00_IN` double DEFAULT NULL,
                                          `SP_DYN_LE00_MA_IN` double DEFAULT NULL,
                                          `SP_DYN_SMAM_FE` double DEFAULT NULL,
                                          `SP_DYN_SMAM_MA` double DEFAULT NULL,
                                          `SP_DYN_TFRT_IN` double DEFAULT NULL,
                                          `SP_DYN_TO65_FE_ZS` double DEFAULT NULL,
                                          `SP_DYN_TO65_MA_ZS` double DEFAULT NULL,
                                          `SP_DYN_WFRT` double DEFAULT NULL,
                                          `SP_HOU_FEMA_ZS` double DEFAULT NULL,
                                          `SP_MTR_1519_ZS` double DEFAULT NULL,
                                          `SP_POP_0004_FE` double DEFAULT NULL,
                                          `SP_POP_0004_FE_5Y` double DEFAULT NULL,
                                          `SP_POP_0004_MA` double DEFAULT NULL,
                                          `SP_POP_0004_MA_5Y` double DEFAULT NULL,
                                          `SP_POP_0014_FE_ZS` double DEFAULT NULL,
                                          `SP_POP_0014_MA_ZS` double DEFAULT NULL,
                                          `SP_POP_0014_TO` double DEFAULT NULL,
                                          `SP_POP_0014_TO_ZS` double DEFAULT NULL,
                                          `SP_POP_0509_FE` double DEFAULT NULL,
                                          `SP_POP_0509_FE_5Y` double DEFAULT NULL,
                                          `SP_POP_0509_MA` double DEFAULT NULL,
                                          `SP_POP_0509_MA_5Y` double DEFAULT NULL,
                                          `SP_POP_1014_FE` double DEFAULT NULL,
                                          `SP_POP_1014_FE_5Y` double DEFAULT NULL,
                                          `SP_POP_1014_MA` double DEFAULT NULL,
                                          `SP_POP_1014_MA_5Y` double DEFAULT NULL,
                                          `SP_POP_1519_FE` double DEFAULT NULL,
                                          `SP_POP_1519_FE_5Y` double DEFAULT NULL,
                                          `SP_POP_1519_MA` double DEFAULT NULL,
                                          `SP_POP_1519_MA_5Y` double DEFAULT NULL,
                                          `SP_POP_1564_FE_ZS` double DEFAULT NULL,
                                          `SP_POP_1564_MA_ZS` double DEFAULT NULL,
                                          `SP_POP_1564_TO` double DEFAULT NULL,
                                          `SP_POP_1564_TO_ZS` double DEFAULT NULL,
                                          `SP_POP_2024_FE` double DEFAULT NULL,
                                          `SP_POP_2024_FE_5Y` double DEFAULT NULL,
                                          `SP_POP_2024_MA` double DEFAULT NULL,
                                          `SP_POP_2024_MA_5Y` double DEFAULT NULL,
                                          `SP_POP_2529_FE` double DEFAULT NULL,
                                          `SP_POP_2529_FE_5Y` double DEFAULT NULL,
                                          `SP_POP_2529_MA` double DEFAULT NULL,
                                          `SP_POP_2529_MA_5Y` double DEFAULT NULL,
                                          `SP_POP_3034_FE` double DEFAULT NULL,
                                          `SP_POP_3034_FE_5Y` double DEFAULT NULL,
                                          `SP_POP_3034_MA` double DEFAULT NULL,
                                          `SP_POP_3034_MA_5Y` double DEFAULT NULL,
                                          `SP_POP_3539_FE` double DEFAULT NULL,
                                          `SP_POP_3539_FE_5Y` double DEFAULT NULL,
                                          `SP_POP_3539_MA` double DEFAULT NULL,
                                          `SP_POP_3539_MA_5Y` double DEFAULT NULL,
                                          `SP_POP_4044_FE` double DEFAULT NULL,
                                          `SP_POP_4044_FE_5Y` double DEFAULT NULL,
                                          `SP_POP_4044_MA` double DEFAULT NULL,
                                          `SP_POP_4044_MA_5Y` double DEFAULT NULL,
                                          `SP_POP_4549_FE` double DEFAULT NULL,
                                          `SP_POP_4549_FE_5Y` double DEFAULT NULL,
                                          `SP_POP_4549_MA` double DEFAULT NULL,
                                          `SP_POP_4549_MA_5Y` double DEFAULT NULL,
                                          `SP_POP_5054_FE` double DEFAULT NULL,
                                          `SP_POP_5054_FE_5Y` double DEFAULT NULL,
                                          `SP_POP_5054_MA` double DEFAULT NULL,
                                          `SP_POP_5054_MA_5Y` double DEFAULT NULL,
                                          `SP_POP_5559_FE` double DEFAULT NULL,
                                          `SP_POP_5559_FE_5Y` double DEFAULT NULL,
                                          `SP_POP_5559_MA` double DEFAULT NULL,
                                          `SP_POP_5559_MA_5Y` double DEFAULT NULL,
                                          `SP_POP_6064_FE` double DEFAULT NULL,
                                          `SP_POP_6064_FE_5Y` double DEFAULT NULL,
                                          `SP_POP_6064_MA` double DEFAULT NULL,
                                          `SP_POP_6064_MA_5Y` double DEFAULT NULL,
                                          `SP_POP_6569_FE` double DEFAULT NULL,
                                          `SP_POP_6569_FE_5Y` double DEFAULT NULL,
                                          `SP_POP_6569_MA` double DEFAULT NULL,
                                          `SP_POP_6569_MA_5Y` double DEFAULT NULL,
                                          `SP_POP_65UP_FE_ZS` double DEFAULT NULL,
                                          `SP_POP_65UP_MA_ZS` double DEFAULT NULL,
                                          `SP_POP_65UP_TO` double DEFAULT NULL,
                                          `SP_POP_65UP_TO_ZS` double DEFAULT NULL,
                                          `SP_POP_7074_FE` double DEFAULT NULL,
                                          `SP_POP_7074_FE_5Y` double DEFAULT NULL,
                                          `SP_POP_7074_MA` double DEFAULT NULL,
                                          `SP_POP_7074_MA_5Y` double DEFAULT NULL,
                                          `SP_POP_7579_FE` double DEFAULT NULL,
                                          `SP_POP_7579_FE_5Y` double DEFAULT NULL,
                                          `SP_POP_7579_MA` double DEFAULT NULL,
                                          `SP_POP_7579_MA_5Y` double DEFAULT NULL,
                                          `SP_POP_80UP_FE` double DEFAULT NULL,
                                          `SP_POP_80UP_FE_5Y` double DEFAULT NULL,
                                          `SP_POP_80UP_MA` double DEFAULT NULL,
                                          `SP_POP_80UP_MA_5Y` double DEFAULT NULL,
                                          `SP_POP_AG00_FE_IN` double DEFAULT NULL,
                                          `SP_POP_AG00_MA_IN` double DEFAULT NULL,
                                          `SP_POP_AG01_FE_IN` double DEFAULT NULL,
                                          `SP_POP_AG01_MA_IN` double DEFAULT NULL,
                                          `SP_POP_AG02_FE_IN` double DEFAULT NULL,
                                          `SP_POP_AG02_MA_IN` double DEFAULT NULL,
                                          `SP_POP_AG03_FE_IN` double DEFAULT NULL,
                                          `SP_POP_AG03_MA_IN` double DEFAULT NULL,
                                          `SP_POP_AG04_FE_IN` double DEFAULT NULL,
                                          `SP_POP_AG04_MA_IN` double DEFAULT NULL,
                                          `SP_POP_AG05_FE_IN` double DEFAULT NULL,
                                          `SP_POP_AG05_MA_IN` double DEFAULT NULL,
                                          `SP_POP_AG06_FE_IN` double DEFAULT NULL,
                                          `SP_POP_AG06_MA_IN` double DEFAULT NULL,
                                          `SP_POP_AG07_FE_IN` double DEFAULT NULL,
                                          `SP_POP_AG07_MA_IN` double DEFAULT NULL,
                                          `SP_POP_AG08_FE_IN` double DEFAULT NULL,
                                          `SP_POP_AG08_MA_IN` double DEFAULT NULL,
                                          `SP_POP_AG09_FE_IN` double DEFAULT NULL,
                                          `SP_POP_AG09_MA_IN` double DEFAULT NULL,
                                          `SP_POP_AG10_FE_IN` double DEFAULT NULL,
                                          `SP_POP_AG10_MA_IN` double DEFAULT NULL,
                                          `SP_POP_AG11_FE_IN` double DEFAULT NULL,
                                          `SP_POP_AG11_MA_IN` double DEFAULT NULL,
                                          `SP_POP_AG12_FE_IN` double DEFAULT NULL,
                                          `SP_POP_AG12_MA_IN` double DEFAULT NULL,
                                          `SP_POP_AG13_FE_IN` double DEFAULT NULL,
                                          `SP_POP_AG13_MA_IN` double DEFAULT NULL,
                                          `SP_POP_AG14_FE_IN` double DEFAULT NULL,
                                          `SP_POP_AG14_MA_IN` double DEFAULT NULL,
                                          `SP_POP_AG15_FE_IN` double DEFAULT NULL,
                                          `SP_POP_AG15_MA_IN` double DEFAULT NULL,
                                          `SP_POP_AG16_FE_IN` double DEFAULT NULL,
                                          `SP_POP_AG16_MA_IN` double DEFAULT NULL,
                                          `SP_POP_AG17_FE_IN` double DEFAULT NULL,
                                          `SP_POP_AG17_MA_IN` double DEFAULT NULL,
                                          `SP_POP_AG18_FE_IN` double DEFAULT NULL,
                                          `SP_POP_AG18_MA_IN` double DEFAULT NULL,
                                          `SP_POP_AG19_FE_IN` double DEFAULT NULL,
                                          `SP_POP_AG19_MA_IN` double DEFAULT NULL,
                                          `SP_POP_AG20_FE_IN` double DEFAULT NULL,
                                          `SP_POP_AG20_MA_IN` double DEFAULT NULL,
                                          `SP_POP_AG21_FE_IN` double DEFAULT NULL,
                                          `SP_POP_AG21_MA_IN` double DEFAULT NULL,
                                          `SP_POP_AG22_FE_IN` double DEFAULT NULL,
                                          `SP_POP_AG22_MA_IN` double DEFAULT NULL,
                                          `SP_POP_AG23_FE_IN` double DEFAULT NULL,
                                          `SP_POP_AG23_MA_IN` double DEFAULT NULL,
                                          `SP_POP_AG24_FE_IN` double DEFAULT NULL,
                                          `SP_POP_AG24_MA_IN` double DEFAULT NULL,
                                          `SP_POP_AG25_FE_IN` double DEFAULT NULL,
                                          `SP_POP_AG25_MA_IN` double DEFAULT NULL,
                                          `SP_POP_BRTH_MF` double DEFAULT NULL,
                                          `SP_POP_DPND` double DEFAULT NULL,
                                          `SP_POP_DPND_OL` double DEFAULT NULL,
                                          `SP_POP_DPND_YG` double DEFAULT NULL,
                                          `SP_POP_GROW` double DEFAULT NULL,
                                          `SP_POP_TOTL` double DEFAULT NULL,
                                          `SP_POP_TOTL_FE_IN` double DEFAULT NULL,
                                          `SP_POP_TOTL_FE_ZS` double DEFAULT NULL,
                                          `SP_POP_TOTL_MA_IN` double DEFAULT NULL,
                                          `SP_POP_TOTL_MA_ZS` double DEFAULT NULL,
                                          `SP_REG_BRTH_RU_ZS` double DEFAULT NULL,
                                          `SP_REG_BRTH_UR_ZS` double DEFAULT NULL,
                                          `SP_REG_BRTH_ZS` double DEFAULT NULL,
                                          `SP_REG_DTHS_ZS` double DEFAULT NULL,
                                          `SP_RUR_TOTL` double DEFAULT NULL,
                                          `SP_RUR_TOTL_ZG` double DEFAULT NULL,
                                          `SP_RUR_TOTL_ZS` double DEFAULT NULL,
                                          `SP_URB_GROW` double DEFAULT NULL,
                                          `SP_URB_TOTL` double DEFAULT NULL,
                                          `SP_URB_TOTL_IN_ZS` double DEFAULT NULL,
                                          `SP_UWT_TFRT` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-18 13:24:33
-- MySQL dump 10.13  Distrib 8.0.25, for macos11 (x86_64)
--
-- Host: localhost    Database: empty
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `rls_filter_roles`
--

DROP TABLE IF EXISTS `rls_filter_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `rls_filter_roles` (
                                    `id` int NOT NULL AUTO_INCREMENT,
                                    `role_id` int NOT NULL,
                                    `rls_filter_id` int DEFAULT NULL,
                                    PRIMARY KEY (`id`),
                                    KEY `rls_filter_id` (`rls_filter_id`),
                                    KEY `role_id` (`role_id`),
                                    CONSTRAINT `rls_filter_roles_ibfk_1` FOREIGN KEY (`rls_filter_id`) REFERENCES `row_level_security_filters` (`id`),
                                    CONSTRAINT `rls_filter_roles_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `ab_role` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-18 13:24:39
-- MySQL dump 10.13  Distrib 8.0.25, for macos11 (x86_64)
--
-- Host: localhost    Database: empty
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `dashboard_user`
--

DROP TABLE IF EXISTS `dashboard_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `dashboard_user` (
                                  `id` int NOT NULL AUTO_INCREMENT,
                                  `user_id` int DEFAULT NULL,
                                  `dashboard_id` int DEFAULT NULL,
                                  PRIMARY KEY (`id`),
                                  KEY `dashboard_id` (`dashboard_id`),
                                  KEY `user_id` (`user_id`),
                                  CONSTRAINT `dashboard_user_ibfk_1` FOREIGN KEY (`dashboard_id`) REFERENCES `dashboards` (`id`),
                                  CONSTRAINT `dashboard_user_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `ab_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-18 13:24:38
-- MySQL dump 10.13  Distrib 8.0.25, for macos11 (x86_64)
--
-- Host: localhost    Database: empty
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `dynamic_plugin`
--

DROP TABLE IF EXISTS `dynamic_plugin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `dynamic_plugin` (
                                  `created_on` datetime DEFAULT NULL,
                                  `changed_on` datetime DEFAULT NULL,
                                  `id` int NOT NULL AUTO_INCREMENT,
                                  `name` varchar(50) NOT NULL,
                                  `key` varchar(50) NOT NULL,
                                  `bundle_url` varchar(1000) NOT NULL,
                                  `created_by_fk` int DEFAULT NULL,
                                  `changed_by_fk` int DEFAULT NULL,
                                  PRIMARY KEY (`id`),
                                  UNIQUE KEY `key` (`key`),
                                  UNIQUE KEY `name` (`name`),
                                  KEY `changed_by_fk` (`changed_by_fk`),
                                  KEY `created_by_fk` (`created_by_fk`),
                                  CONSTRAINT `dynamic_plugin_ibfk_1` FOREIGN KEY (`changed_by_fk`) REFERENCES `ab_user` (`id`),
                                  CONSTRAINT `dynamic_plugin_ibfk_2` FOREIGN KEY (`created_by_fk`) REFERENCES `ab_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-18 13:24:40
-- MySQL dump 10.13  Distrib 8.0.25, for macos11 (x86_64)
--
-- Host: localhost    Database: empty
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `dbs`
--

DROP TABLE IF EXISTS `dbs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `dbs` (
                       `created_on` datetime DEFAULT NULL,
                       `changed_on` datetime DEFAULT NULL,
                       `id` int NOT NULL AUTO_INCREMENT,
                       `database_name` varchar(250) NOT NULL,
                       `sqlalchemy_uri` varchar(1024) NOT NULL,
                       `created_by_fk` int DEFAULT NULL,
                       `changed_by_fk` int DEFAULT NULL,
                       `password` blob,
                       `cache_timeout` int DEFAULT NULL,
                       `extra` text,
                       `select_as_create_table_as` tinyint(1) DEFAULT NULL,
                       `allow_ctas` tinyint(1) DEFAULT NULL,
                       `expose_in_sqllab` tinyint(1) DEFAULT NULL,
                       `force_ctas_schema` varchar(250) DEFAULT NULL,
                       `allow_run_async` tinyint(1) DEFAULT NULL,
                       `allow_dml` tinyint(1) DEFAULT NULL,
                       `verbose_name` varchar(250) DEFAULT NULL,
                       `impersonate_user` tinyint(1) DEFAULT NULL,
                       `allow_multi_schema_metadata_fetch` tinyint(1) DEFAULT NULL,
                       `allow_csv_upload` tinyint(1) NOT NULL DEFAULT '1',
                       `encrypted_extra` blob,
                       `server_cert` blob,
                       `allow_cvas` tinyint(1) DEFAULT NULL,
                       `uuid` binary(16) DEFAULT NULL,
                       `configuration_method` varchar(255) DEFAULT 'sqlalchemy_form',
                       PRIMARY KEY (`id`),
                       UNIQUE KEY `database_name` (`database_name`),
                       UNIQUE KEY `verbose_name` (`verbose_name`),
                       UNIQUE KEY `uq_dbs_uuid` (`uuid`),
                       KEY `created_by_fk` (`created_by_fk`),
                       KEY `changed_by_fk` (`changed_by_fk`),
                       CONSTRAINT `dbs_ibfk_1` FOREIGN KEY (`created_by_fk`) REFERENCES `ab_user` (`id`),
                       CONSTRAINT `dbs_ibfk_2` FOREIGN KEY (`changed_by_fk`) REFERENCES `ab_user` (`id`),
                       CONSTRAINT `dbs_chk_1` CHECK ((`select_as_create_table_as` in (0,1))),
                       CONSTRAINT `dbs_chk_10` CHECK ((`allow_cvas` in (0,1))),
                       CONSTRAINT `dbs_chk_2` CHECK ((`allow_ctas` in (0,1))),
                       CONSTRAINT `dbs_chk_3` CHECK ((`expose_in_sqllab` in (0,1))),
                       CONSTRAINT `dbs_chk_4` CHECK ((`allow_run_async` in (0,1))),
                       CONSTRAINT `dbs_chk_6` CHECK ((`allow_dml` in (0,1))),
                       CONSTRAINT `dbs_chk_7` CHECK ((`impersonate_user` in (0,1))),
                       CONSTRAINT `dbs_chk_8` CHECK ((`allow_multi_schema_metadata_fetch` in (0,1))),
                       CONSTRAINT `dbs_chk_9` CHECK ((`allow_csv_upload` in (0,1)))
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-18 13:24:31
-- MySQL dump 10.13  Distrib 8.0.25, for macos11 (x86_64)
--
-- Host: localhost    Database: empty
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `report_schedule`
--

DROP TABLE IF EXISTS `report_schedule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `report_schedule` (
                                   `id` int NOT NULL AUTO_INCREMENT,
                                   `type` varchar(50) NOT NULL,
                                   `name` varchar(150) NOT NULL,
                                   `description` text,
                                   `context_markdown` text,
                                   `active` tinyint(1) DEFAULT NULL,
                                   `crontab` varchar(1000) NOT NULL,
                                   `sql` text,
                                   `chart_id` int DEFAULT NULL,
                                   `dashboard_id` int DEFAULT NULL,
                                   `database_id` int DEFAULT NULL,
                                   `last_eval_dttm` datetime DEFAULT NULL,
                                   `last_state` varchar(50) DEFAULT NULL,
                                   `last_value` float DEFAULT NULL,
                                   `last_value_row_json` text,
                                   `validator_type` varchar(100) DEFAULT NULL,
                                   `validator_config_json` text,
                                   `log_retention` int DEFAULT NULL,
                                   `grace_period` int DEFAULT NULL,
                                   `created_on` datetime DEFAULT NULL,
                                   `changed_on` datetime DEFAULT NULL,
                                   `created_by_fk` int DEFAULT NULL,
                                   `changed_by_fk` int DEFAULT NULL,
                                   `working_timeout` int DEFAULT NULL,
                                   `report_format` varchar(50) DEFAULT 'PNG',
                                   `creation_method` varchar(255) DEFAULT 'alerts_reports',
                                   `timezone` varchar(100) NOT NULL DEFAULT 'UTC',
                                   PRIMARY KEY (`id`),
                                   UNIQUE KEY `uq_report_schedule_name_type` (`name`,`type`),
                                   KEY `chart_id` (`chart_id`),
                                   KEY `dashboard_id` (`dashboard_id`),
                                   KEY `database_id` (`database_id`),
                                   KEY `changed_by_fk` (`changed_by_fk`),
                                   KEY `created_by_fk` (`created_by_fk`),
                                   KEY `ix_report_schedule_active` (`active`),
                                   KEY `ix_creation_method` (`creation_method`),
                                   CONSTRAINT `report_schedule_ibfk_1` FOREIGN KEY (`chart_id`) REFERENCES `slices` (`id`),
                                   CONSTRAINT `report_schedule_ibfk_2` FOREIGN KEY (`dashboard_id`) REFERENCES `dashboards` (`id`),
                                   CONSTRAINT `report_schedule_ibfk_3` FOREIGN KEY (`database_id`) REFERENCES `dbs` (`id`),
                                   CONSTRAINT `report_schedule_ibfk_4` FOREIGN KEY (`changed_by_fk`) REFERENCES `ab_user` (`id`),
                                   CONSTRAINT `report_schedule_ibfk_5` FOREIGN KEY (`created_by_fk`) REFERENCES `ab_user` (`id`),
                                   CONSTRAINT `report_schedule_chk_1` CHECK ((`active` in (0,1)))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-18 13:24:29
-- MySQL dump 10.13  Distrib 8.0.25, for macos11 (x86_64)
--
-- Host: localhost    Database: empty
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `San Franciso BART Lines`
--

DROP TABLE IF EXISTS `San Franciso BART Lines`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `San Franciso BART Lines` (
                                           `name` varchar(255) DEFAULT NULL,
                                           `color` varchar(255) DEFAULT NULL,
                                           `path_json` text,
                                           `polyline` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-18 13:24:35
-- MySQL dump 10.13  Distrib 8.0.25, for macos11 (x86_64)
--
-- Host: localhost    Database: empty
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `report_recipient`
--

DROP TABLE IF EXISTS `report_recipient`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `report_recipient` (
                                    `id` int NOT NULL AUTO_INCREMENT,
                                    `type` varchar(50) NOT NULL,
                                    `recipient_config_json` text,
                                    `report_schedule_id` int NOT NULL,
                                    `created_on` datetime DEFAULT NULL,
                                    `changed_on` datetime DEFAULT NULL,
                                    `created_by_fk` int DEFAULT NULL,
                                    `changed_by_fk` int DEFAULT NULL,
                                    PRIMARY KEY (`id`),
                                    KEY `report_schedule_id` (`report_schedule_id`),
                                    KEY `changed_by_fk` (`changed_by_fk`),
                                    KEY `created_by_fk` (`created_by_fk`),
                                    CONSTRAINT `report_recipient_ibfk_1` FOREIGN KEY (`report_schedule_id`) REFERENCES `report_schedule` (`id`),
                                    CONSTRAINT `report_recipient_ibfk_2` FOREIGN KEY (`changed_by_fk`) REFERENCES `ab_user` (`id`),
                                    CONSTRAINT `report_recipient_ibfk_3` FOREIGN KEY (`created_by_fk`) REFERENCES `ab_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-18 13:24:33
-- MySQL dump 10.13  Distrib 8.0.25, for macos11 (x86_64)
--
-- Host: localhost    Database: empty
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `slice_email_schedules`
--

DROP TABLE IF EXISTS `slice_email_schedules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `slice_email_schedules` (
                                         `created_on` datetime DEFAULT NULL,
                                         `changed_on` datetime DEFAULT NULL,
                                         `id` int NOT NULL AUTO_INCREMENT,
                                         `active` tinyint(1) DEFAULT NULL,
                                         `crontab` varchar(50) DEFAULT NULL,
                                         `recipients` text,
                                         `deliver_as_group` tinyint(1) DEFAULT NULL,
                                         `delivery_type` enum('attachment','inline') DEFAULT NULL,
                                         `slice_id` int DEFAULT NULL,
                                         `email_format` enum('visualization','data') DEFAULT NULL,
                                         `created_by_fk` int DEFAULT NULL,
                                         `changed_by_fk` int DEFAULT NULL,
                                         `user_id` int DEFAULT NULL,
                                         `slack_channel` text,
                                         `uuid` binary(16) DEFAULT NULL,
                                         PRIMARY KEY (`id`),
                                         UNIQUE KEY `uq_slice_email_schedules_uuid` (`uuid`),
                                         KEY `changed_by_fk` (`changed_by_fk`),
                                         KEY `created_by_fk` (`created_by_fk`),
                                         KEY `slice_id` (`slice_id`),
                                         KEY `user_id` (`user_id`),
                                         KEY `ix_slice_email_schedules_active` (`active`),
                                         CONSTRAINT `slice_email_schedules_ibfk_1` FOREIGN KEY (`changed_by_fk`) REFERENCES `ab_user` (`id`),
                                         CONSTRAINT `slice_email_schedules_ibfk_2` FOREIGN KEY (`created_by_fk`) REFERENCES `ab_user` (`id`),
                                         CONSTRAINT `slice_email_schedules_ibfk_3` FOREIGN KEY (`slice_id`) REFERENCES `slices` (`id`),
                                         CONSTRAINT `slice_email_schedules_ibfk_4` FOREIGN KEY (`user_id`) REFERENCES `ab_user` (`id`),
                                         CONSTRAINT `slice_email_schedules_chk_1` CHECK ((`active` in (0,1))),
                                         CONSTRAINT `slice_email_schedules_chk_2` CHECK ((`deliver_as_group` in (0,1)))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-18 13:24:35
-- MySQL dump 10.13  Distrib 8.0.25, for macos11 (x86_64)
--
-- Host: localhost    Database: empty
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `alert_owner`
--

DROP TABLE IF EXISTS `alert_owner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `alert_owner` (
                               `id` int NOT NULL AUTO_INCREMENT,
                               `user_id` int DEFAULT NULL,
                               `alert_id` int DEFAULT NULL,
                               PRIMARY KEY (`id`),
                               KEY `alert_id` (`alert_id`),
                               KEY `user_id` (`user_id`),
                               CONSTRAINT `alert_owner_ibfk_1` FOREIGN KEY (`alert_id`) REFERENCES `alerts` (`id`),
                               CONSTRAINT `alert_owner_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `ab_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-18 13:24:31
-- MySQL dump 10.13  Distrib 8.0.25, for macos11 (x86_64)
--
-- Host: localhost    Database: empty
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `COVID Vaccines`
--

DROP TABLE IF EXISTS `COVID Vaccines`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `COVID Vaccines` (
                                  `developer_or_researcher` text,
                                  `country_name` text,
                                  `ioc_country_code` text,
                                  `treatment_vs_vaccine` text,
                                  `product_category` text,
                                  `stage_of_development` text,
                                  `date_last_updated` text,
                                  `anticipated_next_steps` text,
                                  `product_description` text,
                                  `clinical_trials` text,
                                  `funder` text,
                                  `published_results` text,
                                  `clinical_trials_for_other_diseases_or_related_use` text,
                                  `fda_approved_indications` float DEFAULT NULL,
                                  `sources` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-18 13:24:38
-- MySQL dump 10.13  Distrib 8.0.25, for macos11 (x86_64)
--
-- Host: localhost    Database: empty
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tab_state`
--

DROP TABLE IF EXISTS `tab_state`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tab_state` (
                             `created_on` datetime DEFAULT NULL,
                             `changed_on` datetime DEFAULT NULL,
                             `extra_json` text,
                             `id` int NOT NULL AUTO_INCREMENT,
                             `user_id` int DEFAULT NULL,
                             `label` varchar(256) DEFAULT NULL,
                             `active` tinyint(1) DEFAULT NULL,
                             `database_id` int DEFAULT NULL,
                             `schema` varchar(256) DEFAULT NULL,
                             `sql` text,
                             `query_limit` int DEFAULT NULL,
                             `latest_query_id` varchar(11) DEFAULT NULL,
                             `autorun` tinyint(1) NOT NULL,
                             `template_params` text,
                             `created_by_fk` int DEFAULT NULL,
                             `changed_by_fk` int DEFAULT NULL,
                             `hide_left_bar` tinyint(1) NOT NULL DEFAULT '0',
                             PRIMARY KEY (`id`),
                             UNIQUE KEY `ix_tab_state_id` (`id`),
                             KEY `changed_by_fk` (`changed_by_fk`),
                             KEY `created_by_fk` (`created_by_fk`),
                             KEY `database_id` (`database_id`),
                             KEY `latest_query_id` (`latest_query_id`),
                             KEY `user_id` (`user_id`),
                             CONSTRAINT `tab_state_ibfk_1` FOREIGN KEY (`changed_by_fk`) REFERENCES `ab_user` (`id`),
                             CONSTRAINT `tab_state_ibfk_2` FOREIGN KEY (`created_by_fk`) REFERENCES `ab_user` (`id`),
                             CONSTRAINT `tab_state_ibfk_3` FOREIGN KEY (`database_id`) REFERENCES `dbs` (`id`),
                             CONSTRAINT `tab_state_ibfk_4` FOREIGN KEY (`latest_query_id`) REFERENCES `query` (`client_id`),
                             CONSTRAINT `tab_state_ibfk_5` FOREIGN KEY (`user_id`) REFERENCES `ab_user` (`id`),
                             CONSTRAINT `tab_state_chk_1` CHECK ((`active` in (0,1))),
                             CONSTRAINT `tab_state_chk_2` CHECK ((`autorun` in (0,1))),
                             CONSTRAINT `tab_state_chk_3` CHECK ((`hide_left_bar` in (0,1)))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-18 13:24:34
-- MySQL dump 10.13  Distrib 8.0.25, for macos11 (x86_64)
--
-- Host: localhost    Database: empty
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `slice_user`
--

DROP TABLE IF EXISTS `slice_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `slice_user` (
                              `id` int NOT NULL AUTO_INCREMENT,
                              `user_id` int DEFAULT NULL,
                              `slice_id` int DEFAULT NULL,
                              PRIMARY KEY (`id`),
                              KEY `slice_id` (`slice_id`),
                              KEY `user_id` (`user_id`),
                              CONSTRAINT `slice_user_ibfk_1` FOREIGN KEY (`slice_id`) REFERENCES `slices` (`id`),
                              CONSTRAINT `slice_user_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `ab_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=215 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-18 13:24:41
-- MySQL dump 10.13  Distrib 8.0.25, for macos11 (x86_64)
--
-- Host: localhost    Database: empty
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `alembic_version`
--

DROP TABLE IF EXISTS `alembic_version`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `alembic_version` (
                                   `version_num` varchar(32) NOT NULL,
                                   PRIMARY KEY (`version_num`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-18 13:24:36
-- MySQL dump 10.13  Distrib 8.0.25, for macos11 (x86_64)
--
-- Host: localhost    Database: empty
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `keyvalue`
--

DROP TABLE IF EXISTS `keyvalue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `keyvalue` (
                            `id` int NOT NULL AUTO_INCREMENT,
                            `value` text NOT NULL,
                            PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-18 13:24:39
-- MySQL dump 10.13  Distrib 8.0.25, for macos11 (x86_64)
--
-- Host: localhost    Database: empty
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cache_keys`
--

DROP TABLE IF EXISTS `cache_keys`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cache_keys` (
                              `id` int NOT NULL AUTO_INCREMENT,
                              `cache_key` varchar(256) NOT NULL,
                              `cache_timeout` int DEFAULT NULL,
                              `datasource_uid` varchar(64) NOT NULL,
                              `created_on` datetime DEFAULT NULL,
                              PRIMARY KEY (`id`),
                              KEY `ix_cache_keys_datasource_uid` (`datasource_uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-18 13:24:29
-- MySQL dump 10.13  Distrib 8.0.25, for macos11 (x86_64)
--
-- Host: localhost    Database: empty
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Slack Exported Metrics`
--

DROP TABLE IF EXISTS `Slack Exported Metrics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Slack Exported Metrics` (
                                          `date` datetime DEFAULT NULL,
                                          `total_membership` bigint DEFAULT NULL,
                                          `full_members` bigint DEFAULT NULL,
                                          `guests` bigint DEFAULT NULL,
                                          `daily_active_members` bigint DEFAULT NULL,
                                          `daily_members_posting_messages` bigint DEFAULT NULL,
                                          `weekly_active_members` bigint DEFAULT NULL,
                                          `weekly_members_posting_messages` bigint DEFAULT NULL,
                                          `messages_in_public_channels` bigint DEFAULT NULL,
                                          `messages_in_private_channels` bigint DEFAULT NULL,
                                          `messages_in_shared_channels` bigint DEFAULT NULL,
                                          `messages_in_dms` bigint DEFAULT NULL,
                                          `percent_of_messages_public_channels` float DEFAULT NULL,
                                          `percent_of_messages_private_channels` float DEFAULT NULL,
                                          `percent_of_messages_dms` float DEFAULT NULL,
                                          `percent_of_views_public_channels` float DEFAULT NULL,
                                          `percent_of_views_private_channels` float DEFAULT NULL,
                                          `percent_of_views_dms` float DEFAULT NULL,
                                          `name` bigint DEFAULT NULL,
                                          `public_channels_single_workspace` bigint DEFAULT NULL,
                                          `messages_posted` bigint DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-18 13:24:32
-- MySQL dump 10.13  Distrib 8.0.25, for macos11 (x86_64)
--
-- Host: localhost    Database: empty
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `dashboard_slices`
--

DROP TABLE IF EXISTS `dashboard_slices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `dashboard_slices` (
                                    `id` int NOT NULL AUTO_INCREMENT,
                                    `dashboard_id` int DEFAULT NULL,
                                    `slice_id` int DEFAULT NULL,
                                    PRIMARY KEY (`id`),
                                    UNIQUE KEY `uq_dashboard_slice` (`dashboard_id`,`slice_id`),
                                    KEY `slice_id` (`slice_id`),
                                    CONSTRAINT `dashboard_slices_ibfk_1` FOREIGN KEY (`dashboard_id`) REFERENCES `dashboards` (`id`),
                                    CONSTRAINT `dashboard_slices_ibfk_2` FOREIGN KEY (`slice_id`) REFERENCES `slices` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=374 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-18 13:24:42
-- MySQL dump 10.13  Distrib 8.0.25, for macos11 (x86_64)
--
-- Host: localhost    Database: empty
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `birth_france_by_region`
--

DROP TABLE IF EXISTS `birth_france_by_region`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `birth_france_by_region` (
                                          `DEPT_ID` varchar(10) DEFAULT NULL,
                                          `2003` bigint DEFAULT NULL,
                                          `2004` bigint DEFAULT NULL,
                                          `2005` bigint DEFAULT NULL,
                                          `2006` bigint DEFAULT NULL,
                                          `2007` bigint DEFAULT NULL,
                                          `2008` bigint DEFAULT NULL,
                                          `2009` bigint DEFAULT NULL,
                                          `2010` bigint DEFAULT NULL,
                                          `2011` bigint DEFAULT NULL,
                                          `2012` bigint DEFAULT NULL,
                                          `2013` bigint DEFAULT NULL,
                                          `2014` bigint DEFAULT NULL,
                                          `dttm` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-18 13:24:40
-- MySQL dump 10.13  Distrib 8.0.25, for macos11 (x86_64)
--
-- Host: localhost    Database: empty
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Slack Channels`
--

DROP TABLE IF EXISTS `Slack Channels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Slack Channels` (
                                  `channel_id` varchar(255) DEFAULT NULL,
                                  `created` datetime DEFAULT NULL,
                                  `creator` varchar(255) DEFAULT NULL,
                                  `id` varchar(255) DEFAULT NULL,
                                  `is_archived` tinyint(1) DEFAULT NULL,
                                  `is_channel` tinyint(1) DEFAULT NULL,
                                  `is_ext_shared` tinyint(1) DEFAULT NULL,
                                  `is_general` tinyint(1) DEFAULT NULL,
                                  `is_group` tinyint(1) DEFAULT NULL,
                                  `is_im` tinyint(1) DEFAULT NULL,
                                  `is_member` tinyint(1) DEFAULT NULL,
                                  `is_mpim` tinyint(1) DEFAULT NULL,
                                  `is_org_shared` tinyint(1) DEFAULT NULL,
                                  `is_pending_ext_shared` tinyint(1) DEFAULT NULL,
                                  `is_private` tinyint(1) DEFAULT NULL,
                                  `is_shared` tinyint(1) DEFAULT NULL,
                                  `members` text,
                                  `name` varchar(255) DEFAULT NULL,
                                  `name_normalized` varchar(255) DEFAULT NULL,
                                  `num_members` bigint DEFAULT NULL,
                                  `parent_conversation` varchar(255) DEFAULT NULL,
                                  `pending_connected_team_ids` text,
                                  `pending_shared` text,
                                  `previous_names` text,
                                  `purpose__creator` varchar(255) DEFAULT NULL,
                                  `purpose__last_set` datetime DEFAULT NULL,
                                  `purpose__value` varchar(255) DEFAULT NULL,
                                  `shared_team_ids` text,
                                  `topic__creator` varchar(255) DEFAULT NULL,
                                  `topic__last_set` datetime DEFAULT NULL,
                                  `topic__value` text,
                                  `unlinked` datetime DEFAULT NULL,
                                  CONSTRAINT `slack channels_chk_1` CHECK ((`is_archived` in (0,1))),
                                  CONSTRAINT `slack channels_chk_10` CHECK ((`is_pending_ext_shared` in (0,1))),
                                  CONSTRAINT `slack channels_chk_11` CHECK ((`is_private` in (0,1))),
                                  CONSTRAINT `slack channels_chk_12` CHECK ((`is_shared` in (0,1))),
                                  CONSTRAINT `slack channels_chk_2` CHECK ((`is_channel` in (0,1))),
                                  CONSTRAINT `slack channels_chk_3` CHECK ((`is_ext_shared` in (0,1))),
                                  CONSTRAINT `slack channels_chk_4` CHECK ((`is_general` in (0,1))),
                                  CONSTRAINT `slack channels_chk_5` CHECK ((`is_group` in (0,1))),
                                  CONSTRAINT `slack channels_chk_6` CHECK ((`is_im` in (0,1))),
                                  CONSTRAINT `slack channels_chk_7` CHECK ((`is_member` in (0,1))),
                                  CONSTRAINT `slack channels_chk_8` CHECK ((`is_mpim` in (0,1))),
                                  CONSTRAINT `slack channels_chk_9` CHECK ((`is_org_shared` in (0,1)))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-18 13:24:28
-- MySQL dump 10.13  Distrib 8.0.25, for macos11 (x86_64)
--
-- Host: localhost    Database: empty
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `sql_metrics`
--

DROP TABLE IF EXISTS `sql_metrics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sql_metrics` (
                               `created_on` datetime DEFAULT NULL,
                               `changed_on` datetime DEFAULT NULL,
                               `id` int NOT NULL AUTO_INCREMENT,
                               `metric_name` varchar(255) NOT NULL,
                               `verbose_name` varchar(1024) DEFAULT NULL,
                               `metric_type` varchar(32) DEFAULT NULL,
                               `table_id` int DEFAULT NULL,
                               `expression` text NOT NULL,
                               `description` text,
                               `created_by_fk` int DEFAULT NULL,
                               `changed_by_fk` int DEFAULT NULL,
                               `d3format` varchar(128) DEFAULT NULL,
                               `warning_text` text,
                               `extra` text,
                               `uuid` binary(16) DEFAULT NULL,
                               PRIMARY KEY (`id`),
                               UNIQUE KEY `uq_sql_metrics_metric_name` (`metric_name`,`table_id`),
                               UNIQUE KEY `uq_sql_metrics_uuid` (`uuid`),
                               KEY `table_id` (`table_id`),
                               KEY `created_by_fk` (`created_by_fk`),
                               KEY `changed_by_fk` (`changed_by_fk`),
                               CONSTRAINT `sql_metrics_ibfk_1` FOREIGN KEY (`table_id`) REFERENCES `tables` (`id`),
                               CONSTRAINT `sql_metrics_ibfk_2` FOREIGN KEY (`created_by_fk`) REFERENCES `ab_user` (`id`),
                               CONSTRAINT `sql_metrics_ibfk_3` FOREIGN KEY (`changed_by_fk`) REFERENCES `ab_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-18 13:24:43
-- MySQL dump 10.13  Distrib 8.0.25, for macos11 (x86_64)
--
-- Host: localhost    Database: empty
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `access_request`
--

DROP TABLE IF EXISTS `access_request`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `access_request` (
                                  `created_on` datetime DEFAULT NULL,
                                  `changed_on` datetime DEFAULT NULL,
                                  `id` int NOT NULL AUTO_INCREMENT,
                                  `datasource_type` varchar(200) DEFAULT NULL,
                                  `datasource_id` int DEFAULT NULL,
                                  `changed_by_fk` int DEFAULT NULL,
                                  `created_by_fk` int DEFAULT NULL,
                                  PRIMARY KEY (`id`),
                                  KEY `changed_by_fk` (`changed_by_fk`),
                                  KEY `created_by_fk` (`created_by_fk`),
                                  CONSTRAINT `access_request_ibfk_1` FOREIGN KEY (`changed_by_fk`) REFERENCES `ab_user` (`id`),
                                  CONSTRAINT `access_request_ibfk_2` FOREIGN KEY (`created_by_fk`) REFERENCES `ab_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-18 13:24:33
-- MySQL dump 10.13  Distrib 8.0.25, for macos11 (x86_64)
--
-- Host: localhost    Database: empty
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `css_templates`
--

DROP TABLE IF EXISTS `css_templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `css_templates` (
                                 `created_on` datetime DEFAULT NULL,
                                 `changed_on` datetime DEFAULT NULL,
                                 `id` int NOT NULL AUTO_INCREMENT,
                                 `template_name` varchar(250) DEFAULT NULL,
                                 `css` text,
                                 `changed_by_fk` int DEFAULT NULL,
                                 `created_by_fk` int DEFAULT NULL,
                                 PRIMARY KEY (`id`),
                                 KEY `changed_by_fk` (`changed_by_fk`),
                                 KEY `created_by_fk` (`created_by_fk`),
                                 CONSTRAINT `css_templates_ibfk_1` FOREIGN KEY (`changed_by_fk`) REFERENCES `ab_user` (`id`),
                                 CONSTRAINT `css_templates_ibfk_2` FOREIGN KEY (`created_by_fk`) REFERENCES `ab_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-18 13:24:39
-- MySQL dump 10.13  Distrib 8.0.25, for macos11 (x86_64)
--
-- Host: localhost    Database: empty
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `San Francisco Population Polygons`
--

DROP TABLE IF EXISTS `San Francisco Population Polygons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `San Francisco Population Polygons` (
                                                     `zipcode` bigint DEFAULT NULL,
                                                     `population` bigint DEFAULT NULL,
                                                     `area` float DEFAULT NULL,
                                                     `contour` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-18 13:24:36
-- MySQL dump 10.13  Distrib 8.0.25, for macos11 (x86_64)
--
-- Host: localhost    Database: empty
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Video Game Sales`
--

DROP TABLE IF EXISTS `Video Game Sales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Video Game Sales` (
                                    `rank` bigint DEFAULT NULL,
                                    `name` varchar(255) DEFAULT NULL,
                                    `platform` varchar(255) DEFAULT NULL,
                                    `year` bigint DEFAULT NULL,
                                    `genre` varchar(255) DEFAULT NULL,
                                    `publisher` varchar(255) DEFAULT NULL,
                                    `na_sales` float DEFAULT NULL,
                                    `eu_sales` float DEFAULT NULL,
                                    `jp_sales` float DEFAULT NULL,
                                    `other_sales` float DEFAULT NULL,
                                    `global_sales` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-18 13:24:30
-- MySQL dump 10.13  Distrib 8.0.25, for macos11 (x86_64)
--
-- Host: localhost    Database: empty
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Flights`
--

DROP TABLE IF EXISTS `Flights`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Flights` (
                           `DAY_OF_WEEK` bigint DEFAULT NULL,
                           `AIRLINE` text,
                           `FLIGHT_NUMBER` bigint DEFAULT NULL,
                           `TAIL_NUMBER` text,
                           `ORIGIN_AIRPORT` text,
                           `DESTINATION_AIRPORT` text,
                           `SCHEDULED_DEPARTURE` bigint DEFAULT NULL,
                           `DEPARTURE_TIME` double DEFAULT NULL,
                           `DEPARTURE_DELAY` double DEFAULT NULL,
                           `TAXI_OUT` double DEFAULT NULL,
                           `WHEELS_OFF` double DEFAULT NULL,
                           `SCHEDULED_TIME` bigint DEFAULT NULL,
                           `ELAPSED_TIME` double DEFAULT NULL,
                           `AIR_TIME` double DEFAULT NULL,
                           `DISTANCE` bigint DEFAULT NULL,
                           `WHEELS_ON` double DEFAULT NULL,
                           `TAXI_IN` double DEFAULT NULL,
                           `SCHEDULED_ARRIVAL` bigint DEFAULT NULL,
                           `ARRIVAL_TIME` double DEFAULT NULL,
                           `ARRIVAL_DELAY` double DEFAULT NULL,
                           `DIVERTED` bigint DEFAULT NULL,
                           `CANCELLED` bigint DEFAULT NULL,
                           `CANCELLATION_REASON` text,
                           `AIR_SYSTEM_DELAY` double DEFAULT NULL,
                           `SECURITY_DELAY` double DEFAULT NULL,
                           `AIRLINE_DELAY` double DEFAULT NULL,
                           `LATE_AIRCRAFT_DELAY` double DEFAULT NULL,
                           `WEATHER_DELAY` double DEFAULT NULL,
                           `ds` datetime DEFAULT NULL,
                           `AIRPORT` text,
                           `CITY` text,
                           `STATE` text,
                           `COUNTRY` text,
                           `LATITUDE` double DEFAULT NULL,
                           `LONGITUDE` double DEFAULT NULL,
                           `AIRPORT_DEST` text,
                           `CITY_DEST` text,
                           `STATE_DEST` text,
                           `COUNTRY_DEST` text,
                           `LATITUDE_DEST` double DEFAULT NULL,
                           `LONGITUDE_DEST` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-18 13:24:35
-- MySQL dump 10.13  Distrib 8.0.25, for macos11 (x86_64)
--
-- Host: localhost    Database: empty
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `columns`
--

DROP TABLE IF EXISTS `columns`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `columns` (
                           `created_on` datetime DEFAULT NULL,
                           `changed_on` datetime DEFAULT NULL,
                           `id` int NOT NULL AUTO_INCREMENT,
                           `column_name` varchar(255) NOT NULL,
                           `is_active` tinyint(1) DEFAULT NULL,
                           `type` varchar(32) DEFAULT NULL,
                           `groupby` tinyint(1) DEFAULT NULL,
                           `filterable` tinyint(1) DEFAULT NULL,
                           `description` text,
                           `created_by_fk` int DEFAULT NULL,
                           `changed_by_fk` int DEFAULT NULL,
                           `dimension_spec_json` text,
                           `verbose_name` varchar(1024) DEFAULT NULL,
                           `datasource_id` int DEFAULT NULL,
                           `uuid` binary(16) DEFAULT NULL,
                           PRIMARY KEY (`id`),
                           UNIQUE KEY `uq_columns_column_name` (`column_name`,`datasource_id`),
                           UNIQUE KEY `uq_columns_uuid` (`uuid`),
                           KEY `created_by_fk` (`created_by_fk`),
                           KEY `changed_by_fk` (`changed_by_fk`),
                           KEY `fk_columns_datasource_id_datasources` (`datasource_id`),
                           CONSTRAINT `columns_ibfk_1` FOREIGN KEY (`created_by_fk`) REFERENCES `ab_user` (`id`),
                           CONSTRAINT `columns_ibfk_2` FOREIGN KEY (`changed_by_fk`) REFERENCES `ab_user` (`id`),
                           CONSTRAINT `fk_columns_datasource_id_datasources` FOREIGN KEY (`datasource_id`) REFERENCES `datasources` (`id`),
                           CONSTRAINT `columns_chk_1` CHECK ((`is_active` in (0,1))),
                           CONSTRAINT `columns_chk_2` CHECK ((`groupby` in (0,1))),
                           CONSTRAINT `columns_chk_7` CHECK ((`filterable` in (0,1)))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-18 13:24:41
-- MySQL dump 10.13  Distrib 8.0.25, for macos11 (x86_64)
--
-- Host: localhost    Database: empty
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `url`
--

DROP TABLE IF EXISTS `url`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `url` (
                       `created_on` datetime DEFAULT NULL,
                       `changed_on` datetime DEFAULT NULL,
                       `id` int NOT NULL AUTO_INCREMENT,
                       `url` text,
                       `created_by_fk` int DEFAULT NULL,
                       `changed_by_fk` int DEFAULT NULL,
                       PRIMARY KEY (`id`),
                       KEY `changed_by_fk` (`changed_by_fk`),
                       KEY `created_by_fk` (`created_by_fk`),
                       CONSTRAINT `url_ibfk_1` FOREIGN KEY (`changed_by_fk`) REFERENCES `ab_user` (`id`),
                       CONSTRAINT `url_ibfk_2` FOREIGN KEY (`created_by_fk`) REFERENCES `ab_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-18 13:24:43
-- MySQL dump 10.13  Distrib 8.0.25, for macos11 (x86_64)
--
-- Host: localhost    Database: empty
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Sample Geodata`
--

DROP TABLE IF EXISTS `Sample Geodata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Sample Geodata` (
                                  `LON` double DEFAULT NULL,
                                  `LAT` double DEFAULT NULL,
                                  `NUMBER` text,
                                  `STREET` text,
                                  `UNIT` text,
                                  `CITY` double DEFAULT NULL,
                                  `DISTRICT` double DEFAULT NULL,
                                  `REGION` double DEFAULT NULL,
                                  `POSTCODE` bigint DEFAULT NULL,
                                  `ID` double DEFAULT NULL,
                                  `datetime` datetime DEFAULT NULL,
                                  `occupancy` float DEFAULT NULL,
                                  `radius_miles` float DEFAULT NULL,
                                  `geohash` varchar(12) DEFAULT NULL,
                                  `delimited` varchar(60) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-18 13:24:29
-- MySQL dump 10.13  Distrib 8.0.25, for macos11 (x86_64)
--
-- Host: localhost    Database: empty
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Slack Users`
--

DROP TABLE IF EXISTS `Slack Users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Slack Users` (
                               `color` varchar(255) DEFAULT NULL,
                               `deleted` tinyint(1) DEFAULT NULL,
                               `has_2fa` tinyint(1) DEFAULT NULL,
                               `id` varchar(255) DEFAULT NULL,
                               `is_admin` tinyint(1) DEFAULT NULL,
                               `is_app_user` tinyint(1) DEFAULT NULL,
                               `is_bot` tinyint(1) DEFAULT NULL,
                               `is_owner` tinyint(1) DEFAULT NULL,
                               `is_primary_owner` tinyint(1) DEFAULT NULL,
                               `is_restricted` tinyint(1) DEFAULT NULL,
                               `is_ultra_restricted` tinyint(1) DEFAULT NULL,
                               `name` varchar(255) DEFAULT NULL,
                               `real_name` varchar(255) DEFAULT NULL,
                               `team_id` varchar(255) DEFAULT NULL,
                               `tz` varchar(255) DEFAULT NULL,
                               `tz_label` varchar(255) DEFAULT NULL,
                               `tz_offset` bigint DEFAULT NULL,
                               `updated` datetime DEFAULT NULL,
                               CONSTRAINT `slack users_chk_1` CHECK ((`deleted` in (0,1))),
                               CONSTRAINT `slack users_chk_2` CHECK ((`has_2fa` in (0,1))),
                               CONSTRAINT `slack users_chk_3` CHECK ((`is_admin` in (0,1))),
                               CONSTRAINT `slack users_chk_4` CHECK ((`is_app_user` in (0,1))),
                               CONSTRAINT `slack users_chk_5` CHECK ((`is_bot` in (0,1))),
                               CONSTRAINT `slack users_chk_6` CHECK ((`is_owner` in (0,1))),
                               CONSTRAINT `slack users_chk_7` CHECK ((`is_primary_owner` in (0,1))),
                               CONSTRAINT `slack users_chk_8` CHECK ((`is_restricted` in (0,1))),
                               CONSTRAINT `slack users_chk_9` CHECK ((`is_ultra_restricted` in (0,1)))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-18 13:24:35
-- MySQL dump 10.13  Distrib 8.0.25, for macos11 (x86_64)
--
-- Host: localhost    Database: empty
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ab_user_role`
--

DROP TABLE IF EXISTS `ab_user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ab_user_role` (
                                `id` int NOT NULL AUTO_INCREMENT,
                                `user_id` int DEFAULT NULL,
                                `role_id` int DEFAULT NULL,
                                PRIMARY KEY (`id`),
                                UNIQUE KEY `user_id` (`user_id`,`role_id`),
                                KEY `role_id` (`role_id`),
                                CONSTRAINT `ab_user_role_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `ab_user` (`id`),
                                CONSTRAINT `ab_user_role_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `ab_role` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-18 13:24:31
-- MySQL dump 10.13  Distrib 8.0.25, for macos11 (x86_64)
--
-- Host: localhost    Database: empty
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `datasources`
--

DROP TABLE IF EXISTS `datasources`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `datasources` (
                               `created_on` datetime DEFAULT NULL,
                               `changed_on` datetime DEFAULT NULL,
                               `id` int NOT NULL AUTO_INCREMENT,
                               `datasource_name` varchar(255) NOT NULL,
                               `is_featured` tinyint(1) DEFAULT NULL,
                               `is_hidden` tinyint(1) DEFAULT NULL,
                               `description` text,
                               `default_endpoint` text,
                               `created_by_fk` int DEFAULT NULL,
                               `changed_by_fk` int DEFAULT NULL,
                               `offset` int DEFAULT NULL,
                               `cache_timeout` int DEFAULT NULL,
                               `perm` varchar(1000) DEFAULT NULL,
                               `filter_select_enabled` tinyint(1) DEFAULT NULL,
                               `params` varchar(1000) DEFAULT NULL,
                               `fetch_values_from` varchar(100) DEFAULT NULL,
                               `schema_perm` varchar(1000) DEFAULT NULL,
                               `cluster_id` int NOT NULL,
                               `uuid` binary(16) DEFAULT NULL,
                               PRIMARY KEY (`id`),
                               UNIQUE KEY `uq_datasources_cluster_id` (`cluster_id`,`datasource_name`),
                               UNIQUE KEY `uq_datasources_uuid` (`uuid`),
                               KEY `created_by_fk` (`created_by_fk`),
                               KEY `changed_by_fk` (`changed_by_fk`),
                               CONSTRAINT `datasources_ibfk_3` FOREIGN KEY (`created_by_fk`) REFERENCES `ab_user` (`id`),
                               CONSTRAINT `datasources_ibfk_4` FOREIGN KEY (`changed_by_fk`) REFERENCES `ab_user` (`id`),
                               CONSTRAINT `fk_datasources_cluster_id_clusters` FOREIGN KEY (`cluster_id`) REFERENCES `clusters` (`id`),
                               CONSTRAINT `datasources_chk_1` CHECK ((`is_featured` in (0,1))),
                               CONSTRAINT `datasources_chk_2` CHECK ((`is_hidden` in (0,1))),
                               CONSTRAINT `datasources_chk_3` CHECK ((`filter_select_enabled` in (0,1)))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-18 13:24:37
-- MySQL dump 10.13  Distrib 8.0.25, for macos11 (x86_64)
--
-- Host: localhost    Database: empty
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tagged_object`
--

DROP TABLE IF EXISTS `tagged_object`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tagged_object` (
                                 `created_on` datetime DEFAULT NULL,
                                 `changed_on` datetime DEFAULT NULL,
                                 `id` int NOT NULL AUTO_INCREMENT,
                                 `tag_id` int DEFAULT NULL,
                                 `object_id` int DEFAULT NULL,
                                 `object_type` enum('query','chart','dashboard') DEFAULT NULL,
                                 `created_by_fk` int DEFAULT NULL,
                                 `changed_by_fk` int DEFAULT NULL,
                                 PRIMARY KEY (`id`),
                                 KEY `tag_id` (`tag_id`),
                                 KEY `created_by_fk` (`created_by_fk`),
                                 KEY `changed_by_fk` (`changed_by_fk`),
                                 KEY `ix_tagged_object_object_id` (`object_id`),
                                 CONSTRAINT `tagged_object_ibfk_1` FOREIGN KEY (`tag_id`) REFERENCES `tag` (`id`),
                                 CONSTRAINT `tagged_object_ibfk_2` FOREIGN KEY (`created_by_fk`) REFERENCES `ab_user` (`id`),
                                 CONSTRAINT `tagged_object_ibfk_3` FOREIGN KEY (`changed_by_fk`) REFERENCES `ab_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-18 13:24:34
-- MySQL dump 10.13  Distrib 8.0.25, for macos11 (x86_64)
--
-- Host: localhost    Database: empty
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `birth_names`
--

DROP TABLE IF EXISTS `birth_names`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `birth_names` (
                               `ds` datetime DEFAULT NULL,
                               `gender` varchar(16) DEFAULT NULL,
                               `name` varchar(255) DEFAULT NULL,
                               `num` bigint DEFAULT NULL,
                               `state` varchar(10) DEFAULT NULL,
                               `num_boys` bigint DEFAULT NULL,
                               `num_girls` bigint DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-18 13:24:41
-- MySQL dump 10.13  Distrib 8.0.25, for macos11 (x86_64)
--
-- Host: localhost    Database: empty
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Slack Channels and Members`
--

DROP TABLE IF EXISTS `Slack Channels and Members`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Slack Channels and Members` (
                                              `channel_id` varchar(255) DEFAULT NULL,
                                              `user_id` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-18 13:24:40
-- MySQL dump 10.13  Distrib 8.0.25, for macos11 (x86_64)
--
-- Host: localhost    Database: empty
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Slack Users and Channels`
--

DROP TABLE IF EXISTS `Slack Users and Channels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Slack Users and Channels` (
                                            `name` varchar(255) DEFAULT NULL,
                                            `user_id` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-18 13:24:30
-- MySQL dump 10.13  Distrib 8.0.25, for macos11 (x86_64)
--
-- Host: localhost    Database: empty
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `report_schedule_user`
--

DROP TABLE IF EXISTS `report_schedule_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `report_schedule_user` (
                                        `id` int NOT NULL AUTO_INCREMENT,
                                        `user_id` int NOT NULL,
                                        `report_schedule_id` int NOT NULL,
                                        PRIMARY KEY (`id`),
                                        KEY `report_schedule_id` (`report_schedule_id`),
                                        KEY `user_id` (`user_id`),
                                        CONSTRAINT `report_schedule_user_ibfk_1` FOREIGN KEY (`report_schedule_id`) REFERENCES `report_schedule` (`id`),
                                        CONSTRAINT `report_schedule_user_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `ab_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-18 13:24:29
-- MySQL dump 10.13  Distrib 8.0.25, for macos11 (x86_64)
--
-- Host: localhost    Database: empty
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `table_columns`
--

DROP TABLE IF EXISTS `table_columns`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `table_columns` (
                                 `created_on` datetime DEFAULT NULL,
                                 `changed_on` datetime DEFAULT NULL,
                                 `id` int NOT NULL AUTO_INCREMENT,
                                 `table_id` int DEFAULT NULL,
                                 `column_name` varchar(255) NOT NULL,
                                 `is_dttm` tinyint(1) DEFAULT NULL,
                                 `is_active` tinyint(1) DEFAULT NULL,
                                 `type` varchar(32) DEFAULT NULL,
                                 `groupby` tinyint(1) DEFAULT NULL,
                                 `filterable` tinyint(1) DEFAULT NULL,
                                 `description` text,
                                 `created_by_fk` int DEFAULT NULL,
                                 `changed_by_fk` int DEFAULT NULL,
                                 `expression` text,
                                 `verbose_name` varchar(1024) DEFAULT NULL,
                                 `python_date_format` varchar(255) DEFAULT NULL,
                                 `uuid` binary(16) DEFAULT NULL,
                                 PRIMARY KEY (`id`),
                                 UNIQUE KEY `uq_table_columns_column_name` (`column_name`,`table_id`),
                                 UNIQUE KEY `uq_table_columns_uuid` (`uuid`),
                                 KEY `table_id` (`table_id`),
                                 KEY `created_by_fk` (`created_by_fk`),
                                 KEY `changed_by_fk` (`changed_by_fk`),
                                 CONSTRAINT `table_columns_ibfk_1` FOREIGN KEY (`table_id`) REFERENCES `tables` (`id`),
                                 CONSTRAINT `table_columns_ibfk_2` FOREIGN KEY (`created_by_fk`) REFERENCES `ab_user` (`id`),
                                 CONSTRAINT `table_columns_ibfk_3` FOREIGN KEY (`changed_by_fk`) REFERENCES `ab_user` (`id`),
                                 CONSTRAINT `table_columns_chk_1` CHECK ((`is_dttm` in (0,1))),
                                 CONSTRAINT `table_columns_chk_2` CHECK ((`is_active` in (0,1))),
                                 CONSTRAINT `table_columns_chk_3` CHECK ((`groupby` in (0,1))),
                                 CONSTRAINT `table_columns_chk_8` CHECK ((`filterable` in (0,1)))
) ENGINE=InnoDB AUTO_INCREMENT=814 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-18 13:24:40
-- MySQL dump 10.13  Distrib 8.0.25, for macos11 (x86_64)
--
-- Host: localhost    Database: empty
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `clusters`
--

DROP TABLE IF EXISTS `clusters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `clusters` (
                            `created_on` datetime DEFAULT NULL,
                            `changed_on` datetime DEFAULT NULL,
                            `id` int NOT NULL AUTO_INCREMENT,
                            `cluster_name` varchar(250) NOT NULL,
                            `broker_host` varchar(255) DEFAULT NULL,
                            `broker_port` int DEFAULT NULL,
                            `broker_endpoint` varchar(255) DEFAULT NULL,
                            `metadata_last_refreshed` datetime DEFAULT NULL,
                            `created_by_fk` int DEFAULT NULL,
                            `changed_by_fk` int DEFAULT NULL,
                            `cache_timeout` int DEFAULT NULL,
                            `verbose_name` varchar(250) DEFAULT NULL,
                            `broker_pass` blob,
                            `broker_user` varchar(255) DEFAULT NULL,
                            `uuid` binary(16) DEFAULT NULL,
                            PRIMARY KEY (`id`),
                            UNIQUE KEY `cluster_name` (`cluster_name`),
                            UNIQUE KEY `verbose_name` (`verbose_name`),
                            UNIQUE KEY `uq_clusters_uuid` (`uuid`),
                            KEY `created_by_fk` (`created_by_fk`),
                            KEY `changed_by_fk` (`changed_by_fk`),
                            CONSTRAINT `clusters_ibfk_1` FOREIGN KEY (`created_by_fk`) REFERENCES `ab_user` (`id`),
                            CONSTRAINT `clusters_ibfk_2` FOREIGN KEY (`changed_by_fk`) REFERENCES `ab_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-18 13:24:36
-- MySQL dump 10.13  Distrib 8.0.25, for macos11 (x86_64)
--
-- Host: localhost    Database: empty
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `druiddatasource_user`
--

DROP TABLE IF EXISTS `druiddatasource_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `druiddatasource_user` (
                                        `id` int NOT NULL AUTO_INCREMENT,
                                        `user_id` int DEFAULT NULL,
                                        `datasource_id` int DEFAULT NULL,
                                        PRIMARY KEY (`id`),
                                        KEY `datasource_id` (`datasource_id`),
                                        KEY `user_id` (`user_id`),
                                        CONSTRAINT `druiddatasource_user_ibfk_1` FOREIGN KEY (`datasource_id`) REFERENCES `datasources` (`id`),
                                        CONSTRAINT `druiddatasource_user_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `ab_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-18 13:24:30
-- MySQL dump 10.13  Distrib 8.0.25, for macos11 (x86_64)
--
-- Host: localhost    Database: empty
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tables`
--

DROP TABLE IF EXISTS `tables`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tables` (
                          `created_on` datetime DEFAULT NULL,
                          `changed_on` datetime DEFAULT NULL,
                          `id` int NOT NULL AUTO_INCREMENT,
                          `table_name` varchar(250) NOT NULL,
                          `main_dttm_col` varchar(250) DEFAULT NULL,
                          `default_endpoint` text,
                          `database_id` int NOT NULL,
                          `created_by_fk` int DEFAULT NULL,
                          `changed_by_fk` int DEFAULT NULL,
                          `offset` int DEFAULT NULL,
                          `description` text,
                          `is_featured` tinyint(1) DEFAULT NULL,
                          `cache_timeout` int DEFAULT NULL,
                          `schema` varchar(255) DEFAULT NULL,
                          `sql` text,
                          `params` text,
                          `perm` varchar(1000) DEFAULT NULL,
                          `filter_select_enabled` tinyint(1) DEFAULT NULL,
                          `fetch_values_predicate` text,
                          `is_sqllab_view` tinyint(1) DEFAULT '0',
                          `template_params` text,
                          `schema_perm` varchar(1000) DEFAULT NULL,
                          `extra` text,
                          `uuid` binary(16) DEFAULT NULL,
                          PRIMARY KEY (`id`),
                          UNIQUE KEY `uq_tables_uuid` (`uuid`),
                          KEY `database_id` (`database_id`),
                          KEY `created_by_fk` (`created_by_fk`),
                          KEY `changed_by_fk` (`changed_by_fk`),
                          CONSTRAINT `tables_ibfk_1` FOREIGN KEY (`database_id`) REFERENCES `dbs` (`id`),
                          CONSTRAINT `tables_ibfk_2` FOREIGN KEY (`created_by_fk`) REFERENCES `ab_user` (`id`),
                          CONSTRAINT `tables_ibfk_3` FOREIGN KEY (`changed_by_fk`) REFERENCES `ab_user` (`id`),
                          CONSTRAINT `tables_chk_1` CHECK ((`is_featured` in (0,1))),
                          CONSTRAINT `tables_chk_2` CHECK ((`filter_select_enabled` in (0,1))),
                          CONSTRAINT `tables_chk_3` CHECK ((`is_sqllab_view` in (0,1)))
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-18 13:24:37
-- MySQL dump 10.13  Distrib 8.0.25, for macos11 (x86_64)
--
-- Host: localhost    Database: empty
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `favstar`
--

DROP TABLE IF EXISTS `favstar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `favstar` (
                           `id` int NOT NULL AUTO_INCREMENT,
                           `user_id` int DEFAULT NULL,
                           `class_name` varchar(50) DEFAULT NULL,
                           `obj_id` int DEFAULT NULL,
                           `dttm` datetime DEFAULT NULL,
                           PRIMARY KEY (`id`),
                           KEY `user_id` (`user_id`),
                           CONSTRAINT `favstar_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `ab_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-18 13:24:38
-- MySQL dump 10.13  Distrib 8.0.25, for macos11 (x86_64)
--
-- Host: localhost    Database: empty
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `unicode_test`
--

DROP TABLE IF EXISTS `unicode_test`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `unicode_test` (
                                `phrase` varchar(500) DEFAULT NULL,
                                `short_phrase` varchar(10) DEFAULT NULL,
                                `with_missing` varchar(100) DEFAULT NULL,
                                `dttm` date DEFAULT NULL,
                                `value` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-18 13:24:37
-- MySQL dump 10.13  Distrib 8.0.25, for macos11 (x86_64)
--
-- Host: localhost    Database: empty
-- ------------------------------------------------------
-- Server version	8.0.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `rls_filter_tables`
--

DROP TABLE IF EXISTS `rls_filter_tables`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `rls_filter_tables` (
                                     `id` int NOT NULL AUTO_INCREMENT,
                                     `table_id` int DEFAULT NULL,
                                     `rls_filter_id` int DEFAULT NULL,
                                     PRIMARY KEY (`id`),
                                     KEY `rls_filter_id` (`rls_filter_id`),
                                     KEY `table_id` (`table_id`),
                                     CONSTRAINT `rls_filter_tables_ibfk_1` FOREIGN KEY (`rls_filter_id`) REFERENCES `row_level_security_filters` (`id`),
                                     CONSTRAINT `rls_filter_tables_ibfk_2` FOREIGN KEY (`table_id`) REFERENCES `tables` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-11-18 13:24:37
